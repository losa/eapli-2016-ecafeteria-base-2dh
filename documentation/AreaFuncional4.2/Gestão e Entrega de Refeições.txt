title Gest�o e Entrega de Refei��es

User->MainMenu: Gest�o e Entrega de Refei��es
MainMenu->MainMenu: buildManagementAndMealDeliveryMenu()
User->MainMenu: Delivery Meal
MainMenu-->*MealDeliveryAction:execute()
MealDeliveryAction-->*MealDeliveryUI: show()
MealDeliveryUI->+MealDeliveryUI: doShow()

MealDeliveryUI-->*MealDeliveryController: create()
MealDeliveryController->+PersistenceContext: repositories()
PersistenceContext->*RepositoryFactory: create()
PersistenceContext-->-MealDeliveryController: repoFactory
MealDeliveryController->+RepositoryFactory: reservations()
RepositoryFactory->*ReservationRepository: create()
RepositoryFactory-->-MealDeliveryController: reservationRepo

MealDeliveryUI-->-User: User ID
User->+MealDeliveryUI: Insert User ID

MealDeliveryUI->+MealDeliveryController: deliverRevervation(userID)


MealDeliveryController->*AppSettings : AppSettings.instance()
MealDeliveryController->+AppSettings : getSession()
AppSettings->*Session : instance
AppSettings-->-MealDeliveryController : session
MealDeliveryController->+Session : getPOS()
Session->*POS : instance
Session-->-MealDeliveryController : pos

MealDeliveryController->+POS : MealToWork()
POS-->-MealDeliveryController : mealToWork
MealDeliveryController->+POS : DateToWork()
POS-->-MealDeliveryController : dateToWork

MealDeliveryController->+ReservationRepository : getReservation(userID, dateToWork, mealToWork)
ReservationRepository->*Reservation : create()
ReservationRepository-->-MealDeliveryController : reservation
MealDeliveryController->Reservation : setState(DELIVERED)

    

MealDeliveryController->ReservationRepository: save(reservation)
MealDeliveryController-->-MealDeliveryUI: delivered
MealDeliveryUI-->User: show success of reservation delivered

