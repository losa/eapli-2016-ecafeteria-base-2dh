/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.ChangeKitchenAlertLimitController;
import eapli.framework.actions.Action;

public class KitchenAlertBootstrap implements Action {

    @Override
    public boolean execute() {
        register();
        return false;
    }

    /**
     *
     */
    private void register() {
        final ChangeKitchenAlertLimitController controller = new ChangeKitchenAlertLimitController();
        try {
            controller.registerKitchenAlert(75,90);
        } catch (final Exception e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
        }
    }
}
