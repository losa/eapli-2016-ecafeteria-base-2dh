/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.TimePeriod2;
import eapli.util.DateTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JoséDiogo
 */
public class MenuTest {
    
    public MenuTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of isDateInPeriod method, of class Menu.
     */
    @Test
    public void testIsDateInPeriod() throws ParseException {
        System.out.println("isDateInPeriod");
        Menu instance = new Menu();
        
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Calendar start = DateTime.dateToCalendar(df.parse("1-5-2016"));
        Calendar end = DateTime.dateToCalendar(df.parse("7-5-2016"));
        TimePeriod2 period = new TimePeriod2(start, end);
        
        Calendar edge1 = DateTime.dateToCalendar(df.parse("1-5-2016"));
        Calendar edge2 = DateTime.dateToCalendar(df.parse("7-5-2016"));
        Calendar in = DateTime.dateToCalendar(df.parse("5-5-2016"));
        Calendar out = DateTime.dateToCalendar(df.parse("8-5-2016"));

        assertTrue(instance.isDateInPeriod(edge1));
        assertTrue(instance.isDateInPeriod(edge2));
        assertTrue(instance.isDateInPeriod(in));
        assertFalse(instance.isDateInPeriod(out));
    }

    
}
