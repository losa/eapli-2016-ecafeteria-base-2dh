/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.util.DateTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno Stamm
 */
public class MealPlanTest {
    
    public MealPlanTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBeginDateMustNotBeNull() {
        System.out.println("beginning must not be null");
        
        try {
            // creates date for end
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            Calendar date = DateTime.dateToCalendar(df.parse("23-5-2016"));
            
            MealPlan mealPlan = new MealPlan(null, date);
            
        } catch (ParseException e) {
            fail();
        }
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testEndDateMustNotBeNull() {
       System.out.println("ending must not be null");

       try {
           // creates date for end
           SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
           Calendar date = DateTime.dateToCalendar(df.parse("23-5-2016"));

           MealPlan mealPlan = new MealPlan(date, null);

        } catch (ParseException e) {
            fail();
        }
    }
     
    @Test(expected = IllegalArgumentException.class)
    public void testBeginBeforeEnd() {
        System.out.println("beginning before ending");
        
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            Calendar begin = DateTime.dateToCalendar(df.parse("23-5-2016"));
            Calendar end = DateTime.dateToCalendar(df.parse("22-5-2016"));
           
            MealPlan mealPlan = new MealPlan(begin, end);
           
        }catch (ParseException e) {
           fail();
        }
        
    }
    
    @Test
    public void itemQuantityChanged() {
        System.out.println("item quantity must be changed");
        
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            Calendar begin = DateTime.dateToCalendar(df.parse("21-5-2016"));
            Calendar end = DateTime.dateToCalendar(df.parse("25-5-2016"));
           
            MealPlan mealPlan = new MealPlan(begin, end);
            
            DishType dishType = new DishType("name" , "description");
            Price price = new Price(10);
            Dish dish = new Dish("name", 1, 1, price, dishType);
            
            
            
        }catch (ParseException e) {
           fail();
        }
    }
     
}
