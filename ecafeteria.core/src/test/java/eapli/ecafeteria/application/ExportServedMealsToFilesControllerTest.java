/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.mealbooking.Reservation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class ExportServedMealsToFilesControllerTest {
    /**this use case gets information from the database and creates xml/csv files which is hard to test,it is better to check the file itself **/
    
    ExportServedMealsToFilesController controller;
    public ExportServedMealsToFilesControllerTest() {
        controller = new ExportServedMealsToFilesController();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setTimePeriod method, of class ExportServedMealsToFilesController.
     */
    @Test
    public void testSetTimePeriod() throws ParseException {
        System.out.println("setTimePeriod");
        String stringdate = "01-01-2016";
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = format.parse(stringdate);
        List<Reservation> expResult = controller.setTimePeriod(date, date);
        assertEquals(expResult.isEmpty(),false);
    }

    
    
}
