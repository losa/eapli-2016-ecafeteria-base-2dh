/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.Balance;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.persistence.CheckAccountBalanceRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import java.util.EnumSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro Ferreira
 */
public class CheckAccountBalanceControllerTest {
    
    public CheckAccountBalanceControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCurrentBalance method, of class CheckAccountBalanceController.
     */
    @Test
    public void testGetCurrentBalance() {
        System.out.println("getCurrentBalance");
         RepositoryFactory repository = PersistenceContext.repositories();
        CheckAccountBalanceController instance = new CheckAccountBalanceController();
        
        SystemUser su = Session.authenticatedUser();
        CheckAccountBalanceRepository rep= repository.balanceRepository();
        CafeteriaUser cu = rep.getCafeteriaUser(su.id());
        Account a= cu.getAccount();
        Balance b= a.getBalance();        
        double expResult = b.getValue();
        double result = instance.getCurrentBalance();
        assertEquals(expResult, result, 0.0);
    }
    
}
