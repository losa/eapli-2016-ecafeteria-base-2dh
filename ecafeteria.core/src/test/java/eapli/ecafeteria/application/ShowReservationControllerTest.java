/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.MealType;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class ShowReservationControllerTest {

    /**
     * this use case only returns information from the database,so the tests
     * only pass if there is something in the database *
     */
    ShowReservationController controller;

    public ShowReservationControllerTest() {
        controller = new ShowReservationController();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getDishTypes method, of class ShowReservationController.
     */
    @Test
    public void testGetDishTypes() {
        System.out.println("testGetDishTypes");
        List<DishType> expResult = controller.getDishTypes();
        assertEquals(expResult.isEmpty(), false);
    }

    /**
     * Test of getMealTypes method, of class ShowReservationController.
     */
    @Test
    public void testGetMealTypes() {
        System.out.println("getMealTypes");
        MealType[] expResult = controller.getMealTypes();
        assertEquals(expResult.length, 2);

    }

    /**
     * Test of getDishes method, of class ShowReservationController.
     */
    @Test
    public void testGetDishes() {
        System.out.println("testGetDish");
        List<Dish> expResult = controller.getDishes();
        assertEquals(expResult.isEmpty(), false);
    }

    /**
     * Test of selectDishTypes method, of class ShowReservationController.
     */
    @Test
    public void testSelectDishTypes() {
        System.out.println("selectDishTypes");
        List<DishType> dish = controller.getDishTypes();
        List<Reservation> expResult = controller.selectDishTypes(dish.get(0));
        assertEquals(expResult.isEmpty(),false);
    }

    /**
     * Test of selectMealTypes method, of class ShowReservationController.
     */
    @Test
    public void testSelectMealTypes() {
        System.out.println("selectMealTypes");
        MealType[] mealType = controller.getMealTypes();
        List<Reservation> expResult = controller.selectMealTypes(MealType.LUNCH);
        assertEquals(expResult.isEmpty(),false);
    }

    /**
     * Test of selectDish method, of class ShowReservationController.
     */
    @Test
    public void testSelectDish() {
        System.out.println("selectDish");
        List<Dish> dish = controller.getDishes();
        List<Reservation> expResult = controller.selectDish(dish.get(0));
        assertEquals(expResult.isEmpty(),false);
    }

    /**
     * Test of selectDate method, of class ShowReservationController.
     */
    @Test
    public void testSelectDate() throws ParseException {
        System.out.println("selectDish");
        String stringdate = "01-01-2016";
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        Date date = format.parse(stringdate);
        List<Reservation> expResult = controller.selectDate(date);
        assertEquals(expResult.isEmpty(),false);
    }

}
