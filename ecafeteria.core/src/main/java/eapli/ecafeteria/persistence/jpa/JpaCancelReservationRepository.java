/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.mealbooking.ReservationState;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Nuno
 */
public class JpaCancelReservationRepository extends JpaRepository<Reservation, Long> {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    public CafeteriaUser getCafeteriaUserByID(Username id) {
        final String className = "CafetariaUser";
        String where = "e.SYSTEMUSER_USERNAME like "+ id.toString();
        final Query q = entityManager().createQuery("SELECT e FROM " + className + " e WHERE " + where);
        return (CafeteriaUser)q.getSingleResult();
    }
    
    public ArrayList<Reservation> getReservationList(CafeteriaUser cu) {
        final Query q = entityManager().createQuery("SELECT e FROM Reservation e, CafeteriaUser cu WHERE e.user = :cu");
        q.setParameter("cu", cu);
        List<Reservation> lista = q.getResultList();
        return (ArrayList<Reservation>) lista;
    }
    
    public void setReservationState(Reservation r) {
        Long id = r.getId();
        String state = "Canceled";
        final Query q = entityManager().createQuery("UPDATE ReservationState SET State=" + state + " WHERE ID=" + id + ";");
    }
    
    public List<Meal> getAvailableMeals(Date date) {
        final String className = "Meal";
        String where = "1=1";
        final Query q = entityManager().createQuery("SELECT e FROM " + className + " e WHERE " + where);
        List<Meal> list = q.getResultList();
        List<Meal> aRetornar = new ArrayList<>();
        for(Meal l : list){
            if((l.getDate().before(DateTime.dateToCalendar(date)))){
                int hour = DateTime.dateToCalendar(date).get(Calendar.HOUR_OF_DAY);
                int day = DateTime.dateToCalendar(date).get(Calendar.DAY_OF_MONTH);
                if(day == l.getDate().get(Calendar.HOUR_OF_DAY)){
                    if((l.getMealType().equals(MealType.LUNCH) && hour< 10) || (l.getMealType().equals(MealType.DINNER) && hour< 16)){
                        aRetornar.add(l);
                    }
                }else{
                    aRetornar.add(l);
                }
            }
            
        }
        return aRetornar;
    }
}
