/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author tfroi
 */
public class JpaMealRepository extends JpaRepository<Meal, Long> implements MealRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public List<MealPlanItem> getReservation(Date dateTime, MealType mealType) {
        int quantityAvailable;

        Query myMeals = entityManager().createQuery("SELECT e FROM " + Meal.class.getSimpleName() + " e WHERE e.date=:date AND e.mealType= :mealtype");
        myMeals.setParameter("date", dateTime, TemporalType.DATE);
        myMeals.setParameter("mealtype", mealType);

        List<Meal> meals = myMeals.getResultList();
        List<MealPlanItem> mealPlanItem, availableMeals = new ArrayList();

        for (Meal meal : meals) {
            Query quantity = entityManager().createQuery("SELECT e FROM " + MealPlanItem.class.getSimpleName() + " e WHERE "
                    + MealPlanItem.class.getSimpleName() + ".mealplan_id=:meal");
            quantity.setParameter("meal", meal.getIdMeal());
            mealPlanItem = quantity.getResultList();
            for (int i = 0; i < mealPlanItem.size(); i++) {
                availableMeals.add(mealPlanItem.get(i));
            }

        }

        return availableMeals;
    }

    /**
     * Method that searches for the meals of all the menus received by
     * parameter.
     *
     * @param menus menu list.
     * @return list of meals.
     */
    @Override
    public List<Meal> mealsOfMenus(List<Menu> menus) {

        List<Meal> mealList = new ArrayList<>();

        // gets all the meals for each menu
        for (Menu menu : menus) {
            Query meals = entityManager().createQuery("SELECT e FROM " + Meal.class.getSimpleName() + " e WHERE e.menu= :menucheck");
            meals.setParameter("menucheck", menu);

            mealList.addAll(meals.getResultList());
        }

        return mealList;
    }

    /**
     * Method that searches for the meals of all the menus and meal type
     * received by parameter.
     *
     * @param menus menu list.
     * @param type meal type.
     * @return list of meals.
     */
    @Override
    public List<Meal> mealsOfMenusTypeAndDate(List<Menu> menus, MealType type, Calendar date) {

        List<Meal> mealList = new ArrayList<>();

        for (Menu menu : menus) {
            Query meals = entityManager().createQuery("SELECT e FROM " + Meal.class.getSimpleName() + " e WHERE e.menu = :menu AND e.mealType = :mealtype AND e.date = :date");
            meals.setParameter("menu", menu);
            meals.setParameter("mealtype", type);
            meals.setParameter("date", date, TemporalType.DATE);

            mealList.addAll(meals.getResultList());
        }

        return mealList;
    }

    /**
     * Method that searches for the meals of all the menus and date received by
     * parameter.
     *
     * @param menus menu list.
     * @param date date.
     * @return list of meals.
     */
    @Override
    public List<Meal> mealsOfMenusAndDate(List<Menu> menus, Calendar date) {
        List<Meal> mealList = new ArrayList<>();

        for (Menu menu : menus) {
            Query meals = entityManager().createQuery("SELECT e FROM " + Meal.class.getSimpleName() + " e WHERE e.menu = :menu AND e.date = :date");
            meals.setParameter("menu", menu);
            meals.setParameter("date", date, TemporalType.DATE);

            mealList.addAll(meals.getResultList());
        }

        return mealList;
    }

}
