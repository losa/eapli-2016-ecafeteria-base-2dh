/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;

/**
 *
 * @author Vasco
 */
public class JpaDishRepository extends JpaRepository<Dish, Long> implements DishRepository{

    @Override
    protected String persistenceUnitName() {
         return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }
    
}
