
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.persistence.MovementConsultingRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import javax.persistence.Query;

/**
 *
 * @author José Sousa
 */
public class JpaMovementConsultingRepository extends JpaRepository<CafeteriaUser, MecanographicNumber>
        implements MovementConsultingRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public CafeteriaUser getCafeteriaUser(Username id) {
        Query cu = entityManager().createQuery("SELECT cu FROM" + CafeteriaUser.class.getName() + "cu WHERE " + id.toString() + "like cu.SYSTEMUSER_USERNAME");
        CafeteriaUser c;
        c = (CafeteriaUser) cu.getSingleResult();
        return c;
    }

}
