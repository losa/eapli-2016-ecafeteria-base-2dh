/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.cafeteria.LastMinuteMealSale;
import eapli.ecafeteria.persistence.LastMinuteMealSaleRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;

/**
 *
 * @author tfroi
 */
public class JpaLastMinuteMealSaleRepository  extends JpaRepository<LastMinuteMealSale, Long>
        implements LastMinuteMealSaleRepository{

    @Override
    protected String persistenceUnitName() {
       return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }
    
}
