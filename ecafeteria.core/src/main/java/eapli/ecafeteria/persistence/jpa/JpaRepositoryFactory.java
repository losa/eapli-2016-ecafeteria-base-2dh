package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.persistence.CheckAccountBalanceRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.LastMinuteMealSaleRepository;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenusRepository;
import eapli.ecafeteria.persistence.MovementConsultingRepository;
import eapli.ecafeteria.persistence.POSRepository;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.ReservMealRepository;
import eapli.ecafeteria.persistence.ReservationRepository;
import eapli.ecafeteria.persistence.SignupRequestRepository;
import eapli.ecafeteria.persistence.UserRepository;

/**
 *
 * Created by nuno on 21/03/16.
 */
public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public UserRepository users() {
        return new JpaUserRepository();
    }

    @Override
    public JpaDishTypeRepository dishTypes() {
        return new JpaDishTypeRepository();
    }

    @Override
    public DishRepository dish() {
        return new JpaDishRepository();
    }
    
    @Override
    public JpaOrganicUnitRepository organicUnits() {
        return new JpaOrganicUnitRepository();
    }

    @Override
    public JpaCafeteriaUserRepository cafeteriaUsers() {
        return new JpaCafeteriaUserRepository();
    }
    
    @Override
    public SignupRequestRepository signupRequests() {
        return new JpaSignupRequestRepository();
    }

    @Override
    public ReservationRepository reservations() {
        return new JpaReservationRepository();
    }
    
    @Override
    public JpaMealPlanRepository mealPlanning() {
        return new JpaMealPlanRepository();
    }

    @Override
    public CheckAccountBalanceRepository balanceRepository() {
       return new JpaCheckAccountBalanceRepository();
    }
    
    @Override
    public MealRepository meals() {
        return new JpaMealRepository();
    }
    
    @Override
    public MealPlanItemRepository mealPlanItems() {
        return new JpaMealPlanItemRepository();
    }
    
    @Override
    public POSRepository pos() {
        return new JpaPOSRepository();
    }

    @Override
    public ReservMealRepository reservmeals() {
        return new JpaReservMealRepository();
    }

    @Override
    public KitchenAlertRepository kitchenAlert() {
       return new JpaKitchenAlertRepository();
    }

    @Override
    public MenusRepository menus() {
        return new JpaMenusRepository();
    }

    @Override
    public MovementConsultingRepository movementRepository() {
        return new JpaMovementConsultingRepository();
    }

    @Override
    public LastMinuteMealSaleRepository lastminute() {
        return new JpaLastMinuteMealSaleRepository();
    }
}
