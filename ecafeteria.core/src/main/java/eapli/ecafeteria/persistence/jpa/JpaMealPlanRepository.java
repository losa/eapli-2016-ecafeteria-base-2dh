package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.List;
import eapli.ecafeteria.persistence.MealPlanRepository;
import java.util.Calendar;
import javax.persistence.Query;
import javax.persistence.TemporalType;

public class JpaMealPlanRepository extends JpaRepository<MealPlan, Long> implements MealPlanRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public List<MealPlan> editableMealPlans() {

        // Machine's today's day
        Calendar today = Calendar.getInstance();

        Query plans = entityManager().createQuery("SELECT e FROM " + MealPlan.class.getSimpleName() + " e WHERE e.published = false AND "
                + "e.plan_week.start > :todayDate");
        plans.setParameter("todayDate", today, TemporalType.DATE);

        return plans.getResultList();
    }

    @Override
    public MealPlan findUnpublishedMealPlanByDate(Calendar date) {
        Query plans = entityManager().createQuery("SELECT e FROM " + MealPlan.class.getSimpleName()
                + " e WHERE :date BETWEEN e.plan_week.start AND e.plan_week.end AND e.published=false");
        plans.setParameter("date", date, TemporalType.DATE);

        List<MealPlan> mpList = plans.getResultList();

        if (mpList.isEmpty()) {
            return null;
        }
        return mpList.get(0);
    }

    @Override
    public MealPlan findPublishedMealPlanByDate(Calendar date) {
        Query plans = entityManager().createQuery("SELECT e FROM " + MealPlan.class.getSimpleName()
                + " e WHERE :date BETWEEN e.plan_week.start AND e.plan_week.end AND e.published=true");
        plans.setParameter("date", date, TemporalType.DATE);

        List<MealPlan> mpList = plans.getResultList();

        if (mpList.isEmpty()) {
            return null;
        }
        return mpList.get(0);
    }
}
