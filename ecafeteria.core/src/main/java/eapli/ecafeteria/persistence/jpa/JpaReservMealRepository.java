/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.Movement;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.ReservMealRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author JoaoPedro
 */
public class JpaReservMealRepository extends JpaRepository<Reservation, Long>
        implements ReservMealRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public List<Meal> getAvailableMeals(Date date) {
        final String className = "Meal";
        String where = "1=1";
        final Query q = entityManager().createQuery("SELECT e FROM " + className + " e WHERE " + where);
        List<Meal> list = q.getResultList();
        List<Meal> aRetornar = new ArrayList<>();
        for (Meal l : list) {
            if ((l.getDate().before(DateTime.dateToCalendar(date)))) {
                int hour = DateTime.dateToCalendar(date).get(Calendar.HOUR_OF_DAY);
                int day = DateTime.dateToCalendar(date).get(Calendar.DAY_OF_MONTH);
                if (day == l.getDate().get(Calendar.HOUR_OF_DAY)) {
                    if ((l.getMealType().equals(MealType.LUNCH) && hour < 10) || (l.getMealType().equals(MealType.DINNER) && hour < 16)) {
                        aRetornar.add(l);
                    }
                } else {
                    aRetornar.add(l);
                }
            }

        }
        return aRetornar;
    }

    @Override
    public CafeteriaUser getCafetariaUserByID(Username id) {

        final String className = "CafeteriaUser";
        String where = "e.systemUser.username= :user";
        final Query q = entityManager().createQuery("SELECT e FROM " + className + " e WHERE " + where);
        q.setParameter("user", id);
        return (CafeteriaUser) q.getSingleResult();
    }

    @Override
    public void saveInfo(Movement mov, Meal m) {
        entityManager().persist(mov);
        Query q = entityManager().createQuery("SELECT e FROM MealPlanItem e WHERE e.itemMeal = :id");
        q.setParameter("id", m);

        MealPlanItem mpi = (MealPlanItem) q.getSingleResult();


        /*Se o jpa não ficar com a referência na base de dados do objeto*/
        mpi.addReservedQuantity();
        PersistenceContext.repositories().mealPlanItems().save(mpi);
        //entityManager().persist(mpi);
    }

    public Reservation getReservation(String user, Date date, MealType mealType) {
        EntityManager em = entityManagerFactory().createEntityManager();
        Query cu = entityManager().createQuery("SELECT cu FROM CafeteriaUser cu WHERE " + user.toString() + "like cu.systemuser.username");
        CafeteriaUser c;
        c = (CafeteriaUser) cu.getSingleResult();

        final Query q = entityManager().createQuery("SELECT rev "
                + "FROM " + Reservation.class.getSimpleName() + " rev JOIN rev.meal meal "
                + "WHERE meal.date = :mydate AND meal.mealType = :mealtype AND rev.user_mecographicnumber like :mecograpgicnumber");

        q.setParameter("mydate", date, TemporalType.DATE);
        q.setParameter("mealtype", mealType);
        q.setParameter("mecograpgicnumber", c.mecanographicNumber().toString());

        return (Reservation) q.getSingleResult();
    }

    /**
     * Devolve as reservas
     *
     * @param date dia da reserva
     * @param mealType tipo de refeição
     * @return lista de reservas
     */
    public List<Reservation> getReservations(Date date, MealType mealType) {
        List<Reservation> reservations = new ArrayList<>();

        final Query q = entityManager().createQuery("SELECT rev "
                + "FROM " + Reservation.class.getSimpleName() + " rev JOIN rev.meal meal "
                + "WHERE meal.date = :mydate AND meal.mealType = :mealtype");

        q.setParameter("mydate", date, TemporalType.DATE);
        q.setParameter("mealtype", mealType);

        return q.getResultList();
    }

}
