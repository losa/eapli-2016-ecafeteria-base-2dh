/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenusRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author JoséDiogo
 */
public class JpaMenusRepository extends JpaRepository<Menu, Long> implements MenusRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    public Iterable<Menu> unpublishedMenus() {
        return match("e.published = false");
    }

    @Override
    public Iterable<Menu> publishedMenus() {
        return match("e.published = true");
    }

    @Override
    public List<Menu> publishedMenusWithinPeriod(Calendar beginning, Calendar ending) {

        Query menus = entityManager().createQuery("SELECT e FROM " + Menu.class.getSimpleName() + " e WHERE e.published = true AND "
                + "e.period.start >= :initialDate AND e.period.end <= :endingDate");
        menus.setParameter("initialDate", beginning, TemporalType.DATE);
        menus.setParameter("endingDate", ending, TemporalType.DATE);

        return menus.getResultList();
    }
    
    
    @Override
    public List<Menu> publishedMenusFromDate(Calendar date) {

        Query menus = entityManager().createQuery("SELECT e FROM " + Menu.class.getSimpleName() + " e WHERE e.published = true AND "
                + ":date BETWEEN e.period.start AND e.period.end");
        menus.setParameter("date", date, TemporalType.DATE);

        return menus.getResultList();
    }

}
