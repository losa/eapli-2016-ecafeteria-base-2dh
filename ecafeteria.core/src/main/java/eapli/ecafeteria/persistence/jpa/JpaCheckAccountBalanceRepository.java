/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.persistence.CheckAccountBalanceRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import javax.persistence.Query;


/**
 *
 * @author Pedro Ferreira
 */
class JpaCheckAccountBalanceRepository extends JpaRepository<CafeteriaUser, MecanographicNumber>
        implements CheckAccountBalanceRepository {
    

   
    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public CafeteriaUser getCafeteriaUser(Username id) {
       Query cu= entityManager().createQuery("SELECT cu FROM CafeteriaUser cu WHERE :id = cu.systemUser.username");
       cu.setParameter("id", id);
       CafeteriaUser c;
       c=(CafeteriaUser) cu.getSingleResult();
       return c;
    }


    
    
    
}
