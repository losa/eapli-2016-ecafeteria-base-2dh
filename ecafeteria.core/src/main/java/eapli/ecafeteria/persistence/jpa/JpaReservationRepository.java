/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.Delivered;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.ReservationRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Tiago
 */
public class JpaReservationRepository extends JpaRepository<Reservation, Long> implements ReservationRepository {

    /**
     * query to get reservations by given date *
     */
    private final String RESERVATION_BY_DATE = "SELECT r FROM Reservation r JOIN r.meal m WHERE m.date = :mydate";
    /**
     * query to get reservations by given dish*
     */
    private final String RESERVATION_BY_DISH = "SELECT r FROM Reservation r where r.meal.m_dish  = :mydish";
    /**
     * query to get reservations by given dishtype *
     */
    private final String RESERVATION_BY_DISHTYPE = "SELECT r FROM Reservation r where r.meal.m_dish.dishType = :mydishType";
    /**
     * query to get reservations by given mealtype *
     */
    private final String RESERVATION_BY_MEALTYPE = "SELECT r FROM Reservation r where r.meal.mealType = :mymealType";
    /**
     * query to get reservations eaten by given time period *
     */
    private final String RESERVATION_BY_TIME_PERIOD = "SELECT r FROM Reservation r where r.meal.date >= :mystart and r.meal.date <= :myend";// and r.state.state = 'delivered'";

    @Override
    public Reservation getReservation(Long userID, Date dateTime, MealType mealtTime) {
        // TODO : 
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Reservation> getAllReservations(Username userID) {
        EntityManager em = entityManagerFactory().createEntityManager();
        Query cu = entityManager().createQuery("SELECT " + userID.toString() + " FROM CafeteriaUser cu WHERE " + userID.toString() + "like cu.SYSTEMUSER_USERNAME");
        CafeteriaUser c;
        c = (CafeteriaUser) cu.getSingleResult();
        Query q = entityManager().createQuery("Select e from Reservation where Reservation.user_mecographicnumber like " + c.mecanographicNumber().toString());
        List<Reservation> listaReservas = q.getResultList();
        return listaReservas;
    }

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }
    /**
     * Returns reservatiosn by a given date
     * @param date the date to be searched
     * @return list of reservations
     */
    public List<Reservation> getReservationsByDate(Date date) {
        Query query = entityManager().createQuery(RESERVATION_BY_DATE);
        query.setParameter("mydate", date);
        return query.getResultList();

    }
     /**
     * Returns reservatiosn by a given dish
     * @param dish the dsih to be searched
     * @return list of reservations
     */
    public List<Reservation> getReservationsByDish(Dish dish) {
        Query query = entityManager().createQuery(RESERVATION_BY_DISH);
        query.setParameter("mydish", dish);
        return query.getResultList();
    }
 /**
     * Returns reservatiosn by a given dishtype
     * @param dishtype the date to be searched
     * @return list of reservations
     */
    public List<Reservation> getReservationsByDishType(DishType dishType) {
        Query query = entityManager().createQuery(RESERVATION_BY_DISHTYPE);
        query.setParameter("mydishType", dishType);
        return query.getResultList();
    }
 /**
     * Returns reservatiosn by a given mealtype
     * @param meal the date to be searched
     * @return list of reservations
     */
    public List<Reservation> getReservationsByMealType(MealType mealType) {
        Query query = entityManager().createQuery(RESERVATION_BY_MEALTYPE);
        query.setParameter("mymealType", mealType);
        return query.getResultList();
    }
     /**
     * Returns reservatiosn by a given timeperiod
     * @param start the date to be searched
     * @param end the date to be searched
     * @return list of reservations
     */
    public List<Reservation> getReservationsByTimePeriod(Date start, Date end) {
        Query query = entityManager().createQuery(RESERVATION_BY_TIME_PERIOD);
        query.setParameter("mystart", start);
        query.setParameter("myend", end);
        return query.getResultList();
    }

}
