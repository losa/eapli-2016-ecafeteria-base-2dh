
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.persistence.repositories.Repository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author tiago frois
 */
public interface MealRepository extends Repository<Meal, Long> {

    public List<MealPlanItem> getReservation(Date dateTime, MealType mealtType);

    public List<Meal> mealsOfMenus(List<Menu> menus);

    public List<Meal> mealsOfMenusAndDate(List<Menu> menus, Calendar date);

    public List<Meal> mealsOfMenusTypeAndDate(List<Menu> menus, MealType type, Calendar date);
}
