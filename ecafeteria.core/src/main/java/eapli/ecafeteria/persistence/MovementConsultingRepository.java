/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;

/**
 *
 * @author José Sousa
 */
public interface MovementConsultingRepository {
    
    public CafeteriaUser getCafeteriaUser(Username id);
    
}
