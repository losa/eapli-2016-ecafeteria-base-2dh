/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.persistence.repositories.Repository;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author JoséDiogo
 */
public interface MenusRepository extends Repository<Menu, Long> {

    public Iterable<Menu> publishedMenus();

    public Iterable<Menu> unpublishedMenus();

    public List<Menu> publishedMenusWithinPeriod(Calendar beginning, Calendar ending);

    public List<Menu> publishedMenusFromDate(Calendar date);
}
