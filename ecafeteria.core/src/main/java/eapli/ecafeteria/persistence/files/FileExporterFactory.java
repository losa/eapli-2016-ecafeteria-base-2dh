package eapli.ecafeteria.persistence.files;

import eapli.ecafeteria.domain.mealbooking.Reservation;
import java.util.List;

/**
 *
 * @author Pedro Abreu
 */
public class FileExporterFactory {

    /**
     * option for csv
     */
    public final int STRATEGY_CSV = 1;
    
    /**
     * option for xml
     */
    public final int STRATEGY_XML = 2;

    public FileExporterFactory() {

    }

    /**
     * Commands the creation of a file given the option and list of objects
     * @param strategy the option
     * @param reservations the list of reservations
     */
    public void getStrategy(int strategy,List<Reservation> reservations) {
        
        if (strategy == STRATEGY_CSV) {
            FileExporter fileExporter = new CSVFileExporter();
            fileExporter.exportToFile(reservations);
        }
    
        else if (strategy == STRATEGY_XML) {
            FileExporter fileExporter = new XMLFileExporter();
            fileExporter.exportToFile(reservations);
        }
        
       
    }
}
