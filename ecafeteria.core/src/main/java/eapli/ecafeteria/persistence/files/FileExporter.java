/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.files;

import java.util.List;

/**
 *
 * @author pedro
 * @param <T>
 */
public interface FileExporter<T> {
    
   public void exportToFile(List<T> list);
    
}
