package eapli.ecafeteria.persistence.files;

import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author Pedro Abreu
 */
public class XMLFileExporter<T> implements FileExporter<T>{

    /**
     * File extension of this class exporter
     */
    private static final String FILE_EXTENSION = ".xml";

    /**
     * Constructor
     */
    public XMLFileExporter() {

    }

    /**
     * Exports a list of generic objects to a xml file
     *
     * @param list the generic list to be serialized to xml
     */
    public void exportToFile(List<T> list) {
        try {
            String fileName = list.get(0).getClass().getSimpleName() + FILE_EXTENSION;
            WrapperList jaxbList = new WrapperList(list);
            JAXBContext context = JAXBContext.newInstance(WrapperList.class, list.get(0).getClass());
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.marshal(jaxbList, new File(fileName));

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

}
