package eapli.ecafeteria.persistence.files;

import java.util.List;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "List")
public class WrapperList<T> {

   
   @XmlAnyElement
    private List<T> list;

    public WrapperList() {
    }

    public WrapperList(List<T> list) {
        this.list = list;
    }

    
    public List<T> getList() {
        return list;
    }
}
