package eapli.ecafeteria.persistence.files;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author Pedro Abreu
 * @param <T>
 */
public class CSVFileExporter<T> implements FileExporter<T> {

    /**
     * The caracter to cut down the information into cells
     */
    private static final String CSV_SEPARATOR = ";";

    /**
     * The file extension of this class exporter
     */
    private static final String FILE_EXTENSION = ".csv";

    /**
     * The file enconding for this type of file
     */
    private static final String FILE_ENCONDING = "UTF-8";

    /**
     * Constructor
     */
    public CSVFileExporter() {

    }

    /**
     * Exports a generic list of generic objects to a csv file
     *
     * @param list the generic list to be converted
     */
    public void exportToFile(List<T> list) {
        try {
            String fileName = list.get(0).getClass().getSimpleName() + FILE_EXTENSION;
            BufferedWriter buffer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), FILE_ENCONDING));
            //para os nomes
            StringBuilder titles = new StringBuilder();
            titles = objectIntoNames(list.get(0), titles);

            buffer.write(titles.toString());
            buffer.newLine();

            //para atributos
            for (T obj : list) {
                StringBuilder line = new StringBuilder();
                line = objectIntoValues(obj, line);
                buffer.write(line.toString());
                buffer.newLine();
            }

            buffer.flush();
            buffer.close();
        } catch (Exception ex) {

        }
    }

    /**
     * Returns the name of all variables that make the given object
     *
     * @param object the object
     * @return a list of strings with all variable names
     */
    private String[] getFieldsName(T object) {
        Field[] fields = object.getClass().getDeclaredFields();
        String[] fieldName = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            fieldName[i] = fields[i].getName();
        }
        return fieldName;
    }

    /**
     * Recursive method to get all values of the variables that make the object
     *
     * @param object the object
     * @param line stringbuilder object to store all the variables values
     * @return a string with all object values
     */
    private StringBuilder objectIntoValues(T object, StringBuilder line) {
        try {
            String[] fieldName = getFieldsName(object);
            for (int i = 0; i < fieldName.length; i++) {
                Field f = object.getClass().getDeclaredField(fieldName[i]);
                f.setAccessible(true);
                T objectToWrite = (T) f.get(object);
                if (objectToWrite instanceof Integer || objectToWrite instanceof Double || objectToWrite instanceof Float || objectToWrite instanceof Long || objectToWrite instanceof Short || objectToWrite instanceof Character || objectToWrite instanceof Byte || objectToWrite instanceof Boolean || objectToWrite instanceof String || objectToWrite instanceof Enum) {
                    line.append(objectToWrite);
                    line.append(CSV_SEPARATOR);
                } else if (objectToWrite instanceof Calendar || objectToWrite instanceof Date) {
                    Calendar formatedDate = (GregorianCalendar) objectToWrite;
                    SimpleDateFormat df = new SimpleDateFormat();
                    df.applyPattern("dd/MM/yyyy");          
                    line.append(df.format(formatedDate.getTime()));
                    line.append(CSV_SEPARATOR);

                } else {
                    objectIntoValues(objectToWrite, line);
                }
            }
        } catch (Exception ex) {

        }
        return line;
    }

    /**
     * Recursive method to get all names of the variables that make the object
     *
     * @param object the object
     * @param line stringbuilder object to store all the variables names
     * @return a string with all object names
     */
    private StringBuilder objectIntoNames(T object, StringBuilder line) {
        try {
            String[] fieldName = getFieldsName(object);
            for (int i = 0; i < fieldName.length; i++) {
                Field f = object.getClass().getDeclaredField(fieldName[i]);
                f.setAccessible(true);
                T objectToWrite = (T) f.get(object);
                if (objectToWrite instanceof Integer || objectToWrite instanceof Double || objectToWrite instanceof Float || objectToWrite instanceof Long || objectToWrite instanceof Short || objectToWrite instanceof Character || objectToWrite instanceof Byte || objectToWrite instanceof Boolean || objectToWrite instanceof String || objectToWrite instanceof Date || objectToWrite instanceof Enum || objectToWrite instanceof Calendar) {
                    line.append(object.getClass().getSimpleName() + "." + fieldName[i]);
                    line.append(CSV_SEPARATOR);
                } else {
                    objectIntoNames(objectToWrite, line);
                }
            }
        } catch (Exception ex) {

        }
        return line;
    }

}
