/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.framework.persistence.repositories.Repository;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Tiago
 */
public interface ReservationRepository extends Repository<Reservation, Long> {
    
    
    public Reservation getReservation(Long userID, Date dateTime, MealType mealType);
    
    public List<Reservation> getAllReservations(Username userID);    
}
