/**
 *
 */
package eapli.ecafeteria.persistence;

/**
 * @author Paulo Gandra Sousa
 *
 */
public interface RepositoryFactory {

	UserRepository users();
	DishTypeRepository dishTypes();
        DishRepository dish();
        OrganicUnitRepository organicUnits();
        CafeteriaUserRepository cafeteriaUsers();
        SignupRequestRepository signupRequests();
        ReservationRepository reservations();
        MealPlanRepository mealPlanning();
        CheckAccountBalanceRepository balanceRepository();
        ReservMealRepository reservmeals();
        MealRepository meals();
        MealPlanItemRepository mealPlanItems();
        LastMinuteMealSaleRepository lastminute();
        POSRepository pos();
        KitchenAlertRepository kitchenAlert();
        MenusRepository menus();
        MovementConsultingRepository movementRepository();
}
