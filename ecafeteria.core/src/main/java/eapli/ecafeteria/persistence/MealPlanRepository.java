package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.framework.persistence.repositories.Repository;
import java.util.Calendar;
import java.util.List;

public interface MealPlanRepository extends Repository<MealPlan, Long>{

    public MealPlan findUnpublishedMealPlanByDate(Calendar date);
    
    public MealPlan findPublishedMealPlanByDate(Calendar date);

    public List<MealPlan> editableMealPlans();
  
}
