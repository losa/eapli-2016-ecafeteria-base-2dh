/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.Movement;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.repositories.Repository;
import java.util.Date;
import java.util.List;

/**
 *
 * @author JoaoPedro
 */
public interface ReservMealRepository extends Repository<Reservation, Long>{
    
    public List<Meal> getAvailableMeals(Date date);
    
    public CafeteriaUser getCafetariaUserByID(Username id);
    
    public void saveInfo(Movement mov, Meal m);
    
    public Reservation getReservation(String user, Date date, MealType mealType);
    
    public List<Reservation> getReservations(Date date, MealType mealType);
}
