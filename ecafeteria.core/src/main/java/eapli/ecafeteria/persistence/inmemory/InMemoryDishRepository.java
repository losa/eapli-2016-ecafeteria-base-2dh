/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Vasco
 */
public class InMemoryDishRepository extends InMemoryRepository<Dish, Long> implements DishRepository {

    long nextID = 1;
    
    @Override
    protected Long newPK(Dish entity) {
        return ++nextID;
    }
    
}
