/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author 11410
 */
public class InMemoryKitchenAlertRepository extends InMemoryRepository<KitchenAlert, Long> implements KitchenAlertRepository {

    @Override
    protected Long newPK(KitchenAlert entity) {
               // TODO : PRIMARY KEY
        return Long.MAX_VALUE;
    }
}
