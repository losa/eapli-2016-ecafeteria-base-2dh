/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.cafeteria.LastMinuteMealSale;
import eapli.ecafeteria.persistence.LastMinuteMealSaleRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author tfroi
 */
public class InMemoryLastMinuteMealSaleRepository extends InMemoryRepository<LastMinuteMealSale, Long> implements LastMinuteMealSaleRepository{

    @Override
    protected Long newPK(LastMinuteMealSale entity) {
        // TODO : PRIMARY KEY
        return Long.MAX_VALUE;
    }
    
}
