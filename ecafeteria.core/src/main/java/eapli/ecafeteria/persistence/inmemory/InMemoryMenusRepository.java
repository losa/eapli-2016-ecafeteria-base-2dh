package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenusRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class InMemoryMenusRepository extends InMemoryRepository<Menu, Long> implements MenusRepository {

    @Override
    protected Long newPK(Menu menu) {
        // TODO : PRIMARY KEY
        return Long.MAX_VALUE;
    }

    @Override
    public Iterable<Menu> publishedMenus() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Menu> unpublishedMenus() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Menu> publishedMenusWithinPeriod(Calendar beginning, Calendar ending) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Menu> publishedMenusFromDate(Calendar date) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
