/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author tiago frois
 */
public class InMemoryMealRepository extends InMemoryRepository<Meal, Long> implements MealRepository {

    @Override
    protected Long newPK(Meal entity) {
        // TODO : PRIMARY KEY
        return Long.MAX_VALUE;
    }

    @Override
    public List<Meal> mealsOfMenus(List<Menu> menus) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<MealPlanItem> getReservation(Date dateTime, MealType mealtType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Meal> mealsOfMenusTypeAndDate(List<Menu> menus, MealType type, Calendar date) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Meal> mealsOfMenusAndDate(List<Menu> menus, Calendar date) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
