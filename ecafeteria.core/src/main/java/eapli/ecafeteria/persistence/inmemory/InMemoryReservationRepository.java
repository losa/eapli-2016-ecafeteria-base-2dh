package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.persistence.ReservationRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Date;
import java.util.List;

/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryReservationRepository extends InMemoryRepository<Reservation, Long> implements ReservationRepository {

    @Override
    protected Long newPK(Reservation reservation) {
        // TODO : PRIMARY KEY
        return Long.MAX_VALUE;
    }

    @Override
    public Reservation getReservation(Long userID, Date dateTime, MealType mealtType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Reservation> getAllReservations(Username userID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
