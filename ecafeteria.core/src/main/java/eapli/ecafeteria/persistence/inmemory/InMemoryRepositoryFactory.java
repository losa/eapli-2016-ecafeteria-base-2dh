package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.CheckAccountBalanceRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.LastMinuteMealSaleRepository;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.ReservationRepository;
import eapli.ecafeteria.persistence.SignupRequestRepository;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.ReservMealRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenusRepository;
import eapli.ecafeteria.persistence.MovementConsultingRepository;
import eapli.ecafeteria.persistence.POSRepository;


/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    private static UserRepository userRepository = null;
    private static DishTypeRepository dishTypeRepository = null;
    private static OrganicUnitRepository organicUnitRepository = null;
    private static CafeteriaUserRepository cafeteriaUserRepository = null;
    private static SignupRequestRepository signupRequestRepository = null;
    private static ReservationRepository reservationRepository = null;
    private static MealPlanRepository mealPlanningRepository = null;
    private static CheckAccountBalanceRepository balanceRepository = null;
    private static MealRepository mealRepository = null;
    private static MealPlanItemRepository mealPlanItemRepository = null;
    private static LastMinuteMealSaleRepository lastMinuteMeal = null;
    private static POSRepository pos = null;
    private static KitchenAlertRepository kitchenAlertRepository = null;
    private static MenusRepository menusRepository = null;
    private static MovementConsultingRepository movementRepository = null;

    @Override
    public UserRepository users() {
        if (userRepository == null) {
            userRepository = new InMemoryUserRepository();
        }
        return userRepository;
    }

    @Override
    public DishTypeRepository dishTypes() {
        if (dishTypeRepository == null) {
            dishTypeRepository = new InMemoryDishTypeRepository();
        }
        return dishTypeRepository;
    }

    @Override
    public OrganicUnitRepository organicUnits() {
        if (organicUnitRepository == null) {
            organicUnitRepository = new InMemoryOrganicUnitRepository();
        }
        return organicUnitRepository;
    }

    @Override
    public CafeteriaUserRepository cafeteriaUsers() {

        if (cafeteriaUserRepository == null) {
            cafeteriaUserRepository = new InMemoryCafeteriaUserRepository();
        }
        return cafeteriaUserRepository;
    }
    
    @Override
    public SignupRequestRepository signupRequests() {
        if (signupRequestRepository == null) {
            signupRequestRepository = new InMemorySignupRequestRepository();
        }
        return signupRequestRepository;
    }

    @Override
    public ReservationRepository reservations() {
        if (reservationRepository == null) {
            reservationRepository = new InMemoryReservationRepository();
        }
        return reservationRepository;
    }

    @Override
    public MealPlanRepository mealPlanning() {
        if (mealPlanningRepository == null) {
            mealPlanningRepository = new InMemoryMealPlanRepository();
        }
        return mealPlanningRepository;
    }

    @Override
    public CheckAccountBalanceRepository balanceRepository() {
//        if (balanceRepository == null) {
//            balanceRepository = new InMemoryCheckAccountBalanceRepository();
//        }
        return balanceRepository;
    }

    @Override
    public ReservMealRepository reservmeals() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    public MealRepository meals() {
        if (mealRepository == null) {
            mealRepository = new InMemoryMealRepository();
        }
        return mealRepository;
    }

    @Override
    public POSRepository pos() {
        if (pos == null) {
            pos = new InMemoryPOSRepository();
        }
        return pos;
    }

    @Override
    public KitchenAlertRepository kitchenAlert() {
        if(kitchenAlertRepository == null) {
            kitchenAlertRepository = new InMemoryKitchenAlertRepository();
        }
        return kitchenAlertRepository;
    }

    @Override
    public DishRepository dish() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    @Override
    public MenusRepository menus() {
        if(menusRepository == null) {
            menusRepository = new InMemoryMenusRepository();
        }
        return menusRepository;
    }
    
    public MovementConsultingRepository  movementRepository() {
        return null;
    }

    @Override
    public MealPlanItemRepository mealPlanItems() {
        if(mealPlanItemRepository == null) {
            mealPlanItemRepository = new InMemoryMealPlanItemRepository();
        }
        return mealPlanItemRepository;
    }

    @Override
    public LastMinuteMealSaleRepository lastminute() {
        if(lastMinuteMeal == null) {
            lastMinuteMeal = new InMemoryLastMinuteMealSaleRepository();
        }
        return lastMinuteMeal;
    }
}
