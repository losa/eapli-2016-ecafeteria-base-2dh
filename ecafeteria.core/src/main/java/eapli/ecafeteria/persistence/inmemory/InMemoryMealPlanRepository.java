package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.List;
import eapli.ecafeteria.persistence.MealPlanRepository;
import java.util.Calendar;

public class InMemoryMealPlanRepository extends InMemoryRepository<MealPlan, Long> implements MealPlanRepository {

    @Override
    protected Long newPK(MealPlan mealPlanning) {
        // TODO : PRIMARY KEY
        return Long.MAX_VALUE;
    }

    @Override
    public List<MealPlan> editableMealPlans() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MealPlan findUnpublishedMealPlanByDate(Calendar date) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MealPlan findPublishedMealPlanByDate(Calendar date) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
