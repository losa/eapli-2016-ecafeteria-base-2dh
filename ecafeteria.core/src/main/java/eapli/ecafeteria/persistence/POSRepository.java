/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.framework.persistence.repositories.Repository;

/**
 *
 * @author Tiago
 */
public interface POSRepository extends Repository<POS, Long>{
    
    
}
