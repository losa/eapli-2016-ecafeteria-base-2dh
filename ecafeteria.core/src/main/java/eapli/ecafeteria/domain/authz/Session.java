/**
 *
 */
package eapli.ecafeteria.domain.authz;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.cafeteria.POS;
import java.util.UUID;

import eapli.framework.domain.ValueObject;
import java.util.Date;

/**
 * @author Paulo Gandra Sousa
 *
 */
public class Session implements ValueObject {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static SystemUser user;
    private final UUID token;
    private static POS pos;

    public static SystemUser authenticatedUser() {
        return Session.user;
    }

    public Session(SystemUser user) {
        if (user == null) {
            throw new IllegalStateException("user must not be null");
        }
        this.user = user;
        this.token = UUID.randomUUID();
    }

    @Override
    public String toString() {
        return this.user.id() + "@" + this.token;
    }
    
    public static POS getPOS(){
        return Session.pos;
    }
    
    public void closePOS(){
        pos = null;
    }
    
    /**
     * Return the corrent 
     * @return 
     */
    public static SystemUser getUser() {
        return user;
    }
    
    public POS setPos(Date dateToWork, MealType type) {
        this.pos = new POS(dateToWork, type);
        return this.pos;
    }
    
}
