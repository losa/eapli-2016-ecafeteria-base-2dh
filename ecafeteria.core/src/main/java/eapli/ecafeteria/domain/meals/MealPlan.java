package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.TimePeriod2;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class MealPlan implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // list of plan items
    @OneToMany(cascade = CascadeType.ALL)
    private List<MealPlanItem> plan_items;

    // first day of the week
    @Embedded
    private TimePeriod2 plan_week;

    // published state
    private boolean published;

    public MealPlan() {
        // ORM
    }

    /**
     * MealPlan constructor that receives the beginning and end of the week.
     *
     * @param beginWeek week beginning.
     * @param endWeek week end.
     */
    public MealPlan(Calendar beginWeek, Calendar endWeek) {
        if (beginWeek == null || endWeek == null) {
            throw new IllegalArgumentException();
        }

        // If ending of the week is before ending 
        if (beginWeek.compareTo(endWeek) > 0) {
            throw new IllegalArgumentException();
        }

        plan_week = new TimePeriod2(beginWeek, endWeek);
        plan_items = new ArrayList<>();
        published = false;
    }

    /**
     * Returns the MealPlanItem list.
     *
     * @return MealPlanItem list.
     */
    public List<MealPlanItem> itemList() {
        return this.plan_items;
    }

    /**
     * Method that creates a new meal plan item with the data received and adds
     * it to the meal plan's list of items.
     *
     * @param meal meal plan item's meal.
     * @param quantity cooked quantity.
     */
    public void newItem(Meal meal, int quantity) {
        if (meal == null || quantity <= 0) {
            throw new IllegalArgumentException();
        }
        plan_items.add(new MealPlanItem(meal, quantity));
    }

    /**
     * Changes the quantity of planned meals of the MealPlanItem received by
     * parameter. This MealPlanItem is in the MealPlan's item list.
     *
     * @param planItem MealPlanItem.
     * @param quantity quantity planned.
     */
    public void changeQuantity(MealPlanItem planItem, int quantity) {
        if (quantity <= 0) {
            throw new IllegalArgumentException();
        }

        for (MealPlanItem item : plan_items) {
            if (item.equals(planItem)) {
                item.changeQuantity(quantity);
            }
        }
    }

    /**
     * Updates the quantity of cooked meals in a MealPlanItem with the meal
     * received as parameter.
     *
     * @param meal the Meal to compare.
     * @param quantity quantity cooked.
     * @return true if the MealPlainItem is updated successfully, otherwise
     * returns false.
     */
    public boolean updateMealPlanItemCookedQuantity(Meal meal, int quantity) {
        for (MealPlanItem item : plan_items) {
            if (item.updateCookedQuantity(meal, quantity)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Updates the quantity of cooked meals in a MealPlanItem with the meal
     * received as parameter.
     *
     * @param meal the Meal to compare.
     * @param quantity quantity cooked.
     * @return true if the MealPlainItem is updated successfully, otherwise
     * returns false.
     */
    public boolean updateMealPlanItemUnsoldQuantity(Meal meal, int quantity) {
        for (MealPlanItem item : plan_items) {
            if (item.updateUnsoldQuantity(meal, quantity)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks based on the reservations limits if it should alert that the
     * values have been reached.
     *
     * @param target Target Meal to find in this MealPlan
     * @param yellowAlert Value of the yellowAlert
     * @param redAlert Value of the redAlert
     * @return
     */
    public String checkAlarm(Meal target, int yellowAlert, int redAlert) {

        for (MealPlanItem meal : this.plan_items) {
            if (target.equals(meal.meal())) {
                float percent = 0;
                if (meal.quantityCooked() == 0) {
                    return "none";
                } else {
                    percent = (meal.quantityReserved() * 100.0f) / meal.quantityCooked();
                }

                if (percent >= redAlert) {
                    return "red";
                }
                if (percent >= yellowAlert) {
                    return "yellow";
                }
            }
        }
        return "none";
    }

    /**
     * Change published to true
     */
    public void setPublished() {
        this.published = true;
    }

    public void addMealPlanItemWithMealToList(List<MealPlanItem> listItems, List<Meal> listMeals) {
        for (MealPlanItem mp : itemList()) {
            mp.addToList(listItems, listMeals);
        }
    }

    /**
     * Returns info on the MealPlan instance.
     *
     * @return text info.
     */
    @Override
    public String toString() {
        return this.plan_week.toString();
    }
}
