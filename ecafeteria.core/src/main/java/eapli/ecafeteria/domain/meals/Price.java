/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nuno
 */
@Embeddable
@XmlRootElement(name = "Price")
public class Price implements Serializable{
    
    @XmlElement(name = "value")
    private double value = 0;

    public Price() {
    }
    
    public Price(double value) {
        this.value = value;
    }
    
    public double getValue() {
        return value;
    } 

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Price other = (Price) obj;
        
        return this.value==other.value;
    }
    
    
}
