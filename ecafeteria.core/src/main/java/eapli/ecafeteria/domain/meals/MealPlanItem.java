package eapli.ecafeteria.domain.meals;

import java.io.Serializable;
import java.util.List;
import java.util.Observable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class MealPlanItem extends Observable implements Serializable {

    //meal to serve
    @OneToOne(cascade = CascadeType.MERGE)
    private Meal itemMeal;
    //quantity planned
    private int quantityPlanned;
    //quantity cooked
    private int quantityCooked;
    //quantity consumed
    private int quantityConsumed;
    //quantity reserved
    private int quantityReserved;
    //quantity unsold
    private int quantityUnsold;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public MealPlanItem() {
        //ORM
    }

    public MealPlanItem(Meal meal, int quantity) {
        if (meal == null || quantity <= 0) {
            throw new IllegalArgumentException();
        }

        this.itemMeal = meal;
        this.quantityPlanned = quantity;
    }

    public void changeQuantity(int newQuantity) {
        this.quantityPlanned = newQuantity;
    }
    
    public void addReservedQuantity(){
        this.quantityReserved++;
    }

    public boolean updateCookedQuantity(Meal meal, int quantity) {
        if (meal.equals(this.itemMeal)) {
            this.quantityCooked = quantity;
            return true;
        }
        return false;
    }

    public boolean updateUnsoldQuantity(Meal meal, int quantity) {
        if (meal.equals(this.itemMeal)) {
            this.quantityUnsold = quantity;
            return true;
        }
        return false;
    }

    public int quantityCooked() {
        return this.quantityCooked;
    }

    public int quantityReserved() {
        return this.quantityReserved;
    }

    public int quantityUnsold() {
        return this.quantityUnsold;
    }

    public int quantityConsumed() {
        return quantityConsumed;
    }

    public Meal meal() {
        return this.itemMeal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void PurchaseMeal() {
        this.quantityConsumed--;
        if(this.quantityConsumed==0){
            setChanged();
            notifyObservers("This meal("+this.itemMeal.description()+" is no longer available!");
        }
    }

    public int AvailableMeals() {
        return quantityCooked() - quantityReserved();
    }

    public int differenceCookedUnsold() {
        return this.quantityCooked - this.quantityUnsold;
    }

    public int differencePrediction() {
        return this.quantityCooked - differenceCookedUnsold();
    }

    /**
     * Checks if the meal received by parameter is equal to this instance's
     * meal.
     *
     * @param meal meal to compare.
     * @return false if meals are different.
     */
    public boolean containsMeal(Meal meal) {
        if (this.itemMeal.equals(meal)) {
            return true;
        }
        return false;
    }

    /**
     * Adds to a list of MealPlanItem if has meal contained in the list of meals
     * received as parameter
     *
     * @param listItems list of MealPlanItem
     * @param listMeals list of Meal
     */
    public void addToList(List<MealPlanItem> listItems, List<Meal> listMeals) {
        for (Meal m : listMeals) {
            if (containsMeal(m)) {
                listItems.add(this);
            }
        }
    }

    /**
     * Compares this MealPlanItem instance with the object received by
     * parameter.
     *
     * @param obj object to compare.
     * @return True if the object and the instance are identical or the same.
     * False if the object is null, of a different class or not identical.
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        final MealPlanItem item = (MealPlanItem) obj;

        if (item.containsMeal(itemMeal)) {
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return "Meal: " + this.itemMeal.toString() + " quantity: " + this.quantityPlanned;
    }
}
