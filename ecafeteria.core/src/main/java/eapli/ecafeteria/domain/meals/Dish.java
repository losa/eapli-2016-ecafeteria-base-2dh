/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.AggregateRoot;
import eapli.util.Strings;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author i131463
 */
@Entity
@XmlRootElement(name = "dish")
public class Dish implements AggregateRoot<String>, Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true)
    @XmlElement(name = "name")
    private String name;
    
    @XmlElement(name ="dishtype")
    private DishType dishType;
    
    @XmlElement(name = "kcalories")
    private long kcal;
    
    @XmlElement(name = "fatpercentage")
    private int fatPer;
    
    @XmlElement(name = "dishprice")
    private Price price;

    protected Dish() {
        // for ORM
    }

    public Dish(String name, long kcal, int fatPer, Price price, DishType dishType) {
        if (Strings.isNullOrEmpty(name) || dishType.equals(null) || kcal == 0 || price.equals(null) || fatPer == 0) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.dishType = dishType;
        this.fatPer = fatPer;
        this.kcal = kcal;
        this.price = price;
    }

    public long kcal() {
        return this.kcal;
    }

    public int fatPer() {
        return this.fatPer;
    }

    public Price price() {
        return this.price;
    }

    public String type() {
        return this.dishType.id();
    }

    private void changeDish(long kcal, int fatPer, Price price) {
        this.fatPer = fatPer;
        this.kcal = kcal;
        this.price = price;
    }

    @Override
    public boolean sameAs(Object other) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean is(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String id() {
        return this.name;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final Dish other = (Dish) obj;
        return this.kcal == other.kcal
                && this.fatPer == other.fatPer
                && this.name.equals(other.name)
                && this.dishType.equals(other.dishType)
                && this.price.equals(other.price);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
