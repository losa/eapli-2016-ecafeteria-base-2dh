/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.TimePeriod;
import eapli.framework.domain.TimePeriod2;
import eapli.util.DateTime;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JoséDiogo
 */
@Entity
@XmlRootElement(name = "menu")
public class Menu implements Serializable {

    @XmlElement(name = "published")
    private boolean published;

    @XmlElement(name = "description")
    private String description;    
    
    @Id
    @GeneratedValue
    private Long id;

    @Embedded
    @XmlElement(name = "period")
    private TimePeriod2 period;   

    public Menu() {
    }

    public Menu(TimePeriod2 period, String description) {
        if (isPeriodValid()) {
            this.period = period;
            this.description = description;
        }else{
            throw new IllegalArgumentException();
        }

        published = false;
    }

    private boolean isPeriodValid() {
        //TODO
        return true;
    }

    public boolean isPublished() {
        return this.published;
    }
        
    public Long id() {
        return this.id;
    }
    
    public TimePeriod2 period(){
        return this.period;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }
    
    public boolean isDateInPeriod(Calendar date) {
        return (period.start().before(date) && period.end().after(date));
    }
    
    public String description(){
        return this.description;
    }
    
    public boolean publish(){
        this.published = true;
        return published;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final Menu other = (Menu) obj;
        return this.published == other.published
                && this.period.equals(other.period);

    }

}
