/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import javax.persistence.Embeddable;

/**
 *
 * @author JoséVieira
 */

public enum MealType {    
    LUNCH, DINNER;    
}
