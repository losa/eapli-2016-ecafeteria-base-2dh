package eapli.ecafeteria.domain.meals;

import eapli.framework.persistence.DataIntegrityViolationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gisela Osório
 */
@Entity
@XmlRootElement(name = "meal")
public class Meal implements Serializable {
    
    @Embedded
    @XmlElement(name="price")
    private Price price;
    
    @XmlElement(name="mealType")
    private MealType mealType;

    @XmlElement(name="dish")
    private Dish m_dish;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    @XmlElement(name="mealdate")
    private Calendar date;
    
    @ManyToOne
    @XmlElement(name="menu")
    private Menu menu;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idMeal;

    public Meal() {
    }
    
    public Meal(Calendar d, MealType mt, Dish dish, Menu m) throws IllegalArgumentException{
        if(validate(m,d)){
            this.date = d;
            this.mealType = mt;
            this.m_dish = dish;
            this.menu = m;  
            this.price = dish.price();
        }else{
            throw new IllegalArgumentException();
        }
    }

    public Meal(double p, Calendar d, MealType mt) {
        this.price = new Price(p);
        this.date = d;
        this.mealType = mt;
    }

    public Price getPrice() {
        return price;
    }

    public Calendar getDate() {
        return date;
    }

    public MealType getMealType() {
        return mealType;
    }
    
    /**
     * Retorna uma lista de objetos ordenados pela ordem:
     * na primeira posição o object price
     * na segunda o object mealType
     * na terceira o object m_dish
     * e na quarta a date
     * @return lista com a informação de uma meal
     */
    public List<Object> getInfoMeal() {
        List<Object> info = new ArrayList<>();
        info.add(price);
        info.add(mealType);
        info.add(m_dish);
        info.add(date);
        return info;
    }

    public String description() {
        return this.m_dish.id() + " - " + this.m_dish.type();
    }

    public Long getIdMeal() {
        return idMeal;
    }

    public void setIdMeal(Long idMeal) {
        this.idMeal = idMeal;
    }

    private boolean validate(Menu m, Calendar d) {     
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final Meal other = (Meal) obj;
        return this.price.equals(other.price)
                && this.mealType == other.mealType
                && this.m_dish.equals(other.m_dish)
                && this.date.equals(other.date)
                && this.menu.equals(other.menu);
    }
    
    @Override
    public String toString() {
        return this.m_dish.toString() + " - " + this.date.getTime().toString();
    }
}
