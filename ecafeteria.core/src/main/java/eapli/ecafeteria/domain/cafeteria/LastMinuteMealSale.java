/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.ecafeteria.domain.meals.Meal;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author tfroi
 */
@Entity
public class LastMinuteMealSale implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @OneToOne(cascade = CascadeType.MERGE)
    private Meal lmMeal;
    
    private String payment;
    
    private int type;
    
    private String description;
    
    public LastMinuteMealSale() {
    }

    public LastMinuteMealSale( Meal lmMeal, String payment, int type) {
        this.lmMeal = lmMeal;
        this.payment = payment;
        this.type = type;
    }
    
    public void putDescription(){
        if(this.type==1)
            addDescription("Registered User");
        else
            addDescription("UnRegistered User");
    }

    public String getPayment() {
        return payment;
    }

    public int getType() {
        return type;
    }

    public void addLmMeal(Meal lmMeal) {
        this.lmMeal = lmMeal;
    }
    /**
     * Adds the description of the last minute meal
     * @param description either registered user or unregistered user
     */
    public void addDescription(String description) {
        this.description = description;
    }
    /**
     * Adds the payment of the meal
     * @param payment 
     */
    public void addPayment(String payment) {
        this.payment = payment;
    }
    /**
     * Adds the type of meal it is
     * @param type 1 for Registered Users 2 for unregistered Users
     */
    public void addType(int type) {
        this.type = type;
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    
}
