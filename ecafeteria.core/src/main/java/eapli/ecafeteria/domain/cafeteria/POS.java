/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.ecafeteria.domain.meals.MealType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



/**
 *
 * @author Tiago
 */
@Entity
public class POS implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    //Date to work
    @Temporal(TemporalType.DATE)
    private Date dateToWork;
    //Meal to work (Lunch or Dinner)
    private MealType mealToWork;
    
    @Enumerated(EnumType.STRING)
    private POSState state;
    
    /**
     * List of last minute meal Sale
     */
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<LastMinuteMealSale> lastMinuteMeals;
    /**
     * POS construtor
     */
    protected POS() {
    }
    
    /**
     * Complete POS construtor
     */
    public POS(Date dateToWork, MealType mealToWork) {
        this.dateToWork = dateToWork;
        this.mealToWork = mealToWork;
        this.lastMinuteMeals =  new ArrayList();
        this.state = POSState.OPEN;
    }

    public Date DateToWork() {
        return this.dateToWork;
    }

    public MealType MealToWork() {
        return this.mealToWork;
    }
    
    public void setState(POSState state) {
        this.state = state;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }
    /**
     * Adds a last Minute meal 
     * @param lastMinuteMeal 
     */
    public void addLastMinuteMeal(LastMinuteMealSale lastMinuteMeal){
        this.lastMinuteMeals.add(lastMinuteMeal);
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final POS other = (POS) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.dateToWork, other.dateToWork)) {
            return false;
        }
        if (this.mealToWork != other.mealToWork) {
            return false;
        }
        return true;
    }

    
}
