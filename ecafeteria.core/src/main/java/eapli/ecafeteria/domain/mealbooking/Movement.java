/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author José Sousa
 */
@Entity
public class Movement implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    private String description;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar date;
    
    private double movement;

    public Movement() {
    }
    
    public Movement(String description, Calendar date, double movement) {
        this.description = description;
        this.date = date;
        this.movement = movement;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public Calendar getDate() {
        return date;
    }
    
    public void setDate(Calendar date) {
        this.date = date;
    }
    
    public double getMovement() {
        return movement;
    }
    
    public void setMovement(double movement) {
        this.movement = movement;
    }
    
}
