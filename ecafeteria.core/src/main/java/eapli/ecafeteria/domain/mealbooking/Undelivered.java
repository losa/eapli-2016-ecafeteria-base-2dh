/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author JoaoPedro
 */
@Entity
@DiscriminatorValue("ReservationUndelivered")
public class Undelivered extends ReservationState implements Serializable{
    
    public String state = "undelivered";
    
    public Undelivered(){
        
    }
    
    @Override
    public String getState(){
        return this.state;
    }
}
