/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import eapli.util.DateTime;
import java.io.Serializable;

import javax.persistence.Embeddable;

import eapli.util.Strings;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 */
// FIXME Account is almost certainly an entity (part of the CafeteriaUser
// aggregate) and not a value object
@Entity
public class Account implements Serializable {
    
     private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    //Balance of the account
    private Balance balance;
    
    //Account Description
    private String description;
    
    //List of movements
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Movement> movementsList;

    /**
     * Account construtor
     */
    public Account(String description) {
        if (Strings.isNullOrEmpty(description)) {
            throw new IllegalStateException("Account should neither be null nor empty");
        }
        // FIXME validate invariants, i.e., account regular expression
        this.description = description;
        this.balance= new Balance();
        this.movementsList = new ArrayList();
    }

    /**
     * Account construtor
     */
    protected Account() {
        // for ORM
        this.balance= new Balance();
        this.description="Sou uma conta bonita";
        this.movementsList= new ArrayList();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Account)) {
            return false;
        }

        final Account that = (Account) o;

        return this.description.equals(that.description);

    }

    @Override
    public int hashCode() {
        return this.description.hashCode();
    }

    @Override
    public String toString() {
        return this.description;
    }
    
    public Balance getBalance(){
        return this.balance;
    }
    
    public void setBalance(Balance balance){
        this.balance=balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * reservation movement
     * 
     * @param price value
     */
    public void addMovementReservation(double price) {
        String description = "Reservation";
        Calendar date = DateTime.now();
        double movement = price * -1;
        Movement m = new Movement(description, date, movement);   
        this.movementsList.add(m);
    }
    /**
     * Last Minute Sale Movement
     * @param price 
     */
    public void addMovementLastMinute(double price) {
        String description = "LastMinuteBuy";
        Calendar date = DateTime.now();
        double movement = price * -1;
        Movement m = new Movement(description, date, movement);   
        this.movementsList.add(m);
    }
    /**
     * cancels movement
     * 
     * @param price value
     */
    public void addMovementCancel(double price) {
        String description = "Cancel Reservation";
        Calendar date = DateTime.now();
        Movement m = new Movement(description, date, price);
        movementsList.add(m);
    }
    
    /**
     * charge movement
     * 
     * @param charging value
     */
    public void addMovementCharge(double charging) {
        String description = "Charge";
        Calendar date = DateTime.now();
        Movement m = new Movement(description, date, charging);
        movementsList.add(m);
        this.balance.deposit(charging);
    }
    
    /**
     * list of movements
     * 
     * @return list
     */
    public List<Movement> getMovementsList() {
        return this.movementsList;
    }
    
    /**
     * list of movements by date
     * 
     * @param date date of movement
     * @return list
     */
    public List<Movement> getMovementsListByDate(Calendar date) {
        List<Movement> movementsListByDate = new ArrayList();
        for (Movement m : movementsList) {
            if (m.getDate().after(date)) {
                movementsListByDate.add(m);
            }
        }
        return movementsListByDate;
    }
}
