/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import java.io.Serializable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author JoaoPedro
 */
@Entity
@Table(name="ReservationState")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="ReservationType")
@XmlRootElement(name = "reservationstate")

public abstract class ReservationState implements Serializable{
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    
    private Long id;
    
    @XmlElement(name = "state")
    private String state;
    
    public ReservationState(){
        
    }
    
    public abstract String getState();
    
}
