/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import eapli.ecafeteria.domain.meals.Meal;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Observable;
import javax.persistence.JoinColumn;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author JoaoPedro
 */
@Entity
@XmlRootElement(name = "reservation")
public class Reservation extends Observable implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @XmlElement(name = "id")
    private Long id;

    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = CascadeType.MERGE)
    private ReservationState state;

    
    @ManyToOne(cascade = CascadeType.MERGE)
    
    private CafeteriaUser user;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idMeal", referencedColumnName = "idMeal")
    private Meal meal;

    public Reservation(CafeteriaUser user, Meal meal) {
        this.user = user;
        this.meal = meal;
        this.state = new Active();
    }

    public Reservation() {
        state = new Canceled();
    }

    public void setState(ReservationState state) {
        this.state = state;
    }

    public ReservationState getState() {
        return this.state;
    }

    public CafeteriaUser getUser() {
        return this.user;
    }

    public void setUser(CafeteriaUser user) {
        this.user = user;
    }

    public Meal getMeal() {
        return this.meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public String toString() {

        SimpleDateFormat df = new SimpleDateFormat();
        df.applyPattern("dd/MM/yyyy");
       
        return "Reservation " + this.id + " - " + this.state.getState() + " - " + this.meal.description() + " - " + this.meal.getMealType() + " - " + df.format(this.meal.getDate().getTime());
    }

    public Long getId() {
        return this.id;
    }
}
