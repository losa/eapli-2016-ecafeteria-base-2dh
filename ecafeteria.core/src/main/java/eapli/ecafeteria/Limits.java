/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria;

import eapli.ecafeteria.domain.KitchenAlert;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.List;

/**
 *
 * @author Pedro Fernandes 
 */
public final class Limits {
    
    int yellowAlert;
    int redAlert;
    MealPlan targettedMealPlan;
    Meal receivedMeal;
    String result;

    public Limits(Meal receivedMeal) {
        this.receivedMeal = receivedMeal;
        this.getAlerts();
        this.getMealPlanItem();
    }
    
    protected void getAlerts(){
     KitchenAlertRepository repository = PersistenceContext.repositories().kitchenAlert();
        Iterable<KitchenAlert> alerts = repository.all();
        KitchenAlert alert = alerts.iterator().next();
        List<Integer> retorno = alert.currentKitchenAlertLimits();
        this.yellowAlert=retorno.get(0);
        this.redAlert=retorno.get(1);
    }
    
    protected void getMealPlanItem(){
        MealPlanRepository repository = PersistenceContext.repositories().mealPlanning();
        this.targettedMealPlan = repository.findUnpublishedMealPlanByDate(this.receivedMeal.getDate());
        this.result = targettedMealPlan.checkAlarm(this.receivedMeal, this.yellowAlert, this.redAlert);   
    }
    
    public boolean checkRed(){
        if(this.result.compareTo("red")==0)
            return true;
    return false;
    }
    
     public boolean checkYellow(){
        if(this.result.compareTo("yellow")==0)
            return true;
    return false;
    }
}
