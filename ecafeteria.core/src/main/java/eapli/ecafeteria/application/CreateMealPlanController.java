/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenusRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Bruno Stamm
 */
public class CreateMealPlanController implements Controller {

    private MealPlan meal_plan;

    /**
     * Changes the week of the meal plan and returns list of meals available for
     * that period.
     *
     * @param beginWeek beginning of the week.
     * @param endWeek end of the week.
     * 
     * @return list of meals of the published menus of that week.
     */
    public List<Meal> selectWeek(Calendar beginWeek, Calendar endWeek) { 
        
        this.meal_plan = new MealPlan(beginWeek, endWeek);
        
        // gets published menus within that period 
        final MenusRepository menuRepo = PersistenceContext.repositories().menus();
        final List<Menu> menuList = menuRepo.publishedMenusWithinPeriod(beginWeek, endWeek);
        
        // gets meals of the menus found
        final MealRepository mealRepo = PersistenceContext.repositories().meals();
        return mealRepo.mealsOfMenus(menuList);
    }

    /**
     * Adds a new meal plan item to the meal plan with the meal and quantity
     * received.
     *
     * @param meal meal.
     * @param quantity meal quantity.
     */
    public void newMealQuantity(Meal meal, int quantity) {
        meal_plan.newItem(meal, quantity);
    }

    /**
     * Saves the meal plan in the persistence.
     * @throws eapli.framework.persistence.DataIntegrityViolationException
     */
    public void save() throws DataIntegrityViolationException{
        final MealPlanRepository repo = PersistenceContext.repositories().mealPlanning();
        repo.save(meal_plan);
    }

}
