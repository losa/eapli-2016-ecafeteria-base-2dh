package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenusRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.util.DateTime;
import java.util.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class RegisterCookedMealsController implements Controller {

    private MealPlan currentMealPlan;

    /**
     * Returns current date as Calendar
     *
     * @return current date as Calendar
     */
    public Calendar todayDate() {
        return DateTime.now();
    }

    /**
     * Returns the list of meal types
     *
     * @return the list of meal types
     */
    public MealType[] listMealType() {
        return MealType.values();
    }

    /**
     * Returns a list of meals stored in the repositories with the same date and
     * meal type as received by parameter.
     *
     * @param day the day
     * @param type the meal type
     * @return list of meals
     */
    public List<Meal> listMealsFromDayAndType(Calendar day, MealType type) {

        final MenusRepository menuRepo = PersistenceContext.repositories().menus();
        final List<Menu> menuList = menuRepo.publishedMenusFromDate(day);

        if (menuList.isEmpty()) {
            return null;
        }

        final MealRepository mealRepo = PersistenceContext.repositories().meals();
        final List<Meal> listMeals = mealRepo.mealsOfMenusTypeAndDate(menuList, type, day);

        if (listMeals.isEmpty()) {
            return null;
        }

        //FIXME: change to findPublishedMealPlanByDate if state of meal plan is changed when created
        currentMealPlan = PersistenceContext.repositories().mealPlanning().findUnpublishedMealPlanByDate(day);

        if (currentMealPlan == null) {
            return null;
        }

        return listMeals;
    }

    /**
     * Updates the quantity cooked of a meal plan item with the selected meal in
     * the current meal plan
     *
     * @param meal the meal
     * @param quantity the quantity cooked
     * @return true if updated successfully, otherwise returns false
     */
    public boolean updateMealPlanItemCookedQuantity(Meal meal, int quantity) {
        return currentMealPlan.updateMealPlanItemCookedQuantity(meal, quantity);
    }

    /**
     * Changes the current meal plan state to published and saves it in the
     * repository
     */
    public void saveMealPlan() {
        currentMealPlan = PersistenceContext.repositories().mealPlanning().save(currentMealPlan);
    }

}
