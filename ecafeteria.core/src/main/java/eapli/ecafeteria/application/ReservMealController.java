/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.Limits;
import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.Active;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.Movement;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.ReservMealRepository;
import eapli.ecafeteria.persistence.jpa.JpaReservMealRepository;
import eapli.framework.application.Controller;
import java.util.List;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author JoaoPedro
 */
public class ReservMealController extends Observable implements Controller {

    ReservMealRepository repository;
    
    public ReservMealController(){
        repository = new JpaReservMealRepository();
    }
    
    public List<Meal> getAvailableMeals() {
        return repository.getAvailableMeals(new Date());
    }

    public List<Object> getInfo(Meal m) {
        return m.getInfoMeal();
    }

    public void registReservation(Meal m, Observer receivedObserver) {
        this.addObserver(receivedObserver);
        SystemUser su = Session.authenticatedUser();
        Username id = su.id();
        CafeteriaUser cu = repository.getCafetariaUserByID(id);
        Reservation r = new Reservation();
        r.setMeal(m);
        r.setUser(cu);
        r.setState(new Active());
        
        repository.save(r);
        Movement mov = new Movement("Reserva", m.getDate(), m.getPrice().getValue());
        repository.saveInfo(mov, m);
        
        Limits test = new Limits(m);

        //Notify Observers
        if (test.checkRed() == false) {
            if (test.checkYellow() == true) {
                this.setChanged();
                this.notifyObservers("Yellow");
            }
        } else {
            this.setChanged();
            this.notifyObservers("Red");
        }

    }
}
