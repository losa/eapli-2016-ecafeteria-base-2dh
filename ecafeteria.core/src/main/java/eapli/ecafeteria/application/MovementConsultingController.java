/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.Movement;
import eapli.ecafeteria.persistence.MovementConsultingRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author José Sousa
 */
public class MovementConsultingController {
    
    Calendar date;
    
    public MovementConsultingController() {
        
    }
    
    public void setDate(int year, int month, int day) {
        date = DateTime.newCalendar(year, month, day);
    }
    
    public List<Movement> getMovementListByDate() {
          
        Username id = Session.authenticatedUser().id();
        RepositoryFactory repository = PersistenceContext.repositories();
        MovementConsultingRepository rep = repository.movementRepository();
        CafeteriaUser cu = rep.getCafeteriaUser(id);
        return cu.getAccount().getMovementsListByDate(date);
    }
    
}