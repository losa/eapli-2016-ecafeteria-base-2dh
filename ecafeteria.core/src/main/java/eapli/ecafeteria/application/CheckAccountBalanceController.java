/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.Balance;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.persistence.CheckAccountBalanceRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;


/**
 *
 * @author Pedro Ferreira
 */
public class CheckAccountBalanceController {
    
    private SystemUser user;
    private Session session;
    private Username id;
    
    public CheckAccountBalanceController(){
        
    }
    
    
    public double getCurrentBalance(){
        
        user=session.authenticatedUser();
        id=user.id();
        RepositoryFactory repository = PersistenceContext.repositories();
        
        CheckAccountBalanceRepository rep= repository.balanceRepository();
        CafeteriaUser cu = rep.getCafeteriaUser(id);
        Account a= cu.getAccount();
        Balance b= a.getBalance();
        
        return b.getValue();
    }
    
    
}
