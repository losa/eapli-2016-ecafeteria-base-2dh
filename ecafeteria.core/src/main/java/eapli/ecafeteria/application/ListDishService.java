/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author i131463
 */
public class ListDishService {
    
        public Iterable<Dish> allDishes() {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        final DishRepository dishRepository = PersistenceContext.repositories().dish();
        return dishRepository.all();
    }
    
}
