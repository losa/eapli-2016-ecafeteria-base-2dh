package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.ReservationRepository;
import eapli.ecafeteria.persistence.jpa.JpaRepositoryFactory;
import eapli.ecafeteria.persistence.jpa.JpaReservationRepository;
import eapli.framework.application.Controller;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Pedro Abreu
 */
public class ShowReservationController implements Controller {

    /**
     * controller construtor
     */
    public ShowReservationController() {

    }

    /**
     * returns all types of dishes
     *
     * @return a list dish types
     */
    public List<DishType> getDishTypes() {
        DishTypeRepository dishTypeRepository = PersistenceContext.repositories().dishTypes();
        return (List<DishType>) dishTypeRepository.all();

        //  possibilidade de usar este serviço da aplicacao?  
        //  ListDishTypeController controller = new ListDishTypeController();
        //  return (List<DishType>) controller.listDishTypes();
    }

    /**
     * returns all meal types
     *
     * @return a list of meal types
     */
    public MealType[] getMealTypes() {
        return MealType.class.getEnumConstants();
    }

    /**
     * returns all dishes
     *
     * @return a list of dishes
     */
    public List<Dish> getDishes() {
        DishRepository dishRepository = PersistenceContext.repositories().dish();
        return (List<Dish>) dishRepository.all();

    }

    /**
     * returns the reservations given the dish type in the parameter
     *
     * @param dishType the dishtype
     * @return list of reservations
     */
    public List<Reservation> selectDishTypes(DishType dishType) {
        JpaReservationRepository repo = (JpaReservationRepository) PersistenceContext.repositories().reservations();
        return repo.getReservationsByDishType(dishType);
    }

    /**
     * returns the reservations given the meal type in the parameter
     *
     * @param mealType the mealtype
     * @return list of reservations
     */
    public List<Reservation> selectMealTypes(MealType mealType) {
        JpaReservationRepository repo = (JpaReservationRepository) PersistenceContext.repositories().reservations();
        return repo.getReservationsByMealType(mealType);
    }

    /**
     * returns the reservations given the dish in the parameter
     *
     * @param dish the dish
     * @return list of reservations
     */
    public List<Reservation> selectDish(Dish dish) {
        JpaReservationRepository repo = (JpaReservationRepository) PersistenceContext.repositories().reservations();
        return repo.getReservationsByDish(dish);
    }

    /**
     * returns the reservations given the date in the parameter
     *
     * @param date the date
     * @return list of reservations
     */
    public List<Reservation> selectDate(Date date) {
        JpaReservationRepository repo = (JpaReservationRepository) PersistenceContext.repositories().reservations();
        return repo.getReservationsByDate(date);
    }

}
