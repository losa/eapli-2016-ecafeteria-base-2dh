/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.POSRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.ReservationRepository;
import eapli.framework.application.Controller;
import java.util.Date;

/**
 *
 * @author Tiago
 */
public class OpenPOSController implements Controller{
    
    //repository factory
    RepositoryFactory repoFactory;
    //POS repository
    POSRepository posRepo;
    
    /**
     * controller construtor
     */
    public OpenPOSController() {
        this.repoFactory = PersistenceContext.repositories();
        
        this.posRepo = repoFactory.pos();
    }
    
    /**
     * Choose POS date and meal to work.
     * 
     * @param dateToWork the date to work.
     * @param type the meal to work.
     * @return true if successful false if otherwise.
     */
    public POS choosePOSToWork(Date dateToWork, MealType type){
        
        Session session = AppSettings.instance().session();
        
        POS pos = session.setPos(dateToWork, type);
        
        this.posRepo.save(pos);
        
        return pos;
        
    }
    
}

