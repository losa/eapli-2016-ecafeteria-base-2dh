/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.framework.application.Controller;

/**
 *
 * @author Tiago
 */
public class AddFundsController implements Controller{
    
    //repository factory
    RepositoryFactory repoFactory;
    
    //POS repository
    CafeteriaUserRepository cafeteriaUserRepo;
    
    //Cafeteria User
    public CafeteriaUser cafUser;
    
    /**
     * controller construtor
     */
    public AddFundsController() {
        this.repoFactory = PersistenceContext.repositories();
        
        this.cafeteriaUserRepo = repoFactory.cafeteriaUsers();
    }
    
    /**
     * Find user by its id.
     * @param id of user
     * 
     * @return true if user exist, false if not.
     */
    public boolean getUserById(String id){
        
        MecanographicNumber mecNumber = new MecanographicNumber(id);  

        this.cafUser = this.cafeteriaUserRepo.findById(mecNumber);
        
        if(cafUser == null){
              return false;
              
        }else
              return true;
          
    }
    
    /**
     * Add funds.
     * 
     * @param value funds to add
     */
    public void addFunds(double value){
       
       this.cafUser.getAccount().addMovementCharge(value);
       
       this.cafeteriaUserRepo.save(cafUser);
           
    }
    
}
