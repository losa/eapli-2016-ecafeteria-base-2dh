/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.cafeteria.LastMinuteMealSale;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.LastMinuteMealSaleRepository;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.framework.application.Controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tiago Frois
 */
public class LastMinuteMealSaleController implements Controller{

    RepositoryFactory repoFactory;
    //CafeteriaUser repository
    CafeteriaUserRepository cafeteriaUserRepo;
    MealPlanItemRepository mealItemRepo;
    LastMinuteMealSaleRepository lastMinute;
    MealRepository mealRepo;
    //Cafeteria User
    public CafeteriaUser cafUser;
    LastMinuteMealSale lastMinuteMeal;
    List<MealPlanItem> availableMeals;
    List<Meal> meals;
    
    public LastMinuteMealSaleController() {
        this.repoFactory = PersistenceContext.repositories();
        this.mealRepo = repoFactory.meals();
        this.mealItemRepo = repoFactory.mealPlanItems();
        this.lastMinute =  repoFactory.lastminute();
        this.meals = new ArrayList();
        this.lastMinuteMeal= new LastMinuteMealSale();
    }
    /**
     * Returns true if the Mecanographic number corresponds to a User
     * @param user Mecanographic number
     * @return 
     */
    public boolean getUserById(String user){
        MecanographicNumber mecUser = new MecanographicNumber(user);
        this.cafUser = this.cafeteriaUserRepo.findById(mecUser);
        
        return cafUser != null;
    }
    
    public List<Meal> getAvailableMeals(){
        availableMeals =this.mealRepo.getReservation(eapli.ecafeteria.domain.authz.Session.getPOS().DateToWork(), eapli.ecafeteria.domain.authz.Session.getPOS().MealToWork());
        int size = availableMeals.size();
        for (int i = 0; i < size; i++) {
            if(availableMeals.get(i).AvailableMeals()>0)
                meals.add(availableMeals.get(i).meal());
        }
        
        return meals;
    }
    /**
     * Makes a last minute meal sale and returns true if it is possible to sell
     * @param payment id user or a MB card number
     * @param meal
     * @param option
     * @return 
     */
    public boolean lastMinuteSale(String payment,int meal,int option){
        //Account
        if(option==1){
            removeFunds(meals.get(meal).getPrice().getValue());
        }
        this.lastMinuteMeal.addType(meal);
        this.lastMinuteMeal.addLmMeal(availableMeals.get(meal).meal());
        this.lastMinuteMeal.addPayment(payment);
        this.lastMinuteMeal.putDescription();
        //Adds to the POS list
        eapli.ecafeteria.domain.authz.Session.getPOS().addLastMinuteMeal(lastMinuteMeal);
        //Saves into the DATABASE
        this.lastMinute.save(lastMinuteMeal);
        this.availableMeals.get(meal).PurchaseMeal();
        
        this.mealItemRepo.save(availableMeals.get(meal));
        return true;
    }
    
    
    /**
     * Add funds.
     * 
     * @param value funds to add
     */
    public void removeFunds(double value){
       
       this.cafUser.getAccount().addMovementLastMinute(value);
       
       this.cafeteriaUserRepo.save(cafUser);
           
    }
}
