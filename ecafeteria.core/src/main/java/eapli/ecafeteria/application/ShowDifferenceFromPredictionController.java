package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenusRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Marcelo Oliveira
 */
public class ShowDifferenceFromPredictionController implements Controller {

    private MealPlan currentMealPlan;

    /**
     * Returns a list of meal plan items based on the meals from the day
     * received as parameter.
     *
     * @param day the day
     * @return list of meal plan items
     */
    public List<MealPlanItem> mealPlanItemsFromDay(Calendar day) {

        final MenusRepository menuRepo = PersistenceContext.repositories().menus();
        final List<Menu> menuList = menuRepo.publishedMenusFromDate(day);

        if (menuList.isEmpty()) {
            return null;
        }

        final MealRepository mealRepo = PersistenceContext.repositories().meals();
        final List<Meal> listMeals = mealRepo.mealsOfMenusAndDate(menuList,day);

        if (listMeals.isEmpty()) {
            return null;
        }

        currentMealPlan = PersistenceContext.repositories().mealPlanning().findUnpublishedMealPlanByDate(day);

        if (currentMealPlan == null) {
            return null;
        }

        List<MealPlanItem> listItems = new ArrayList<>();
        currentMealPlan.addMealPlanItemWithMealToList(listItems, listMeals);

        return listItems;
    }

}
