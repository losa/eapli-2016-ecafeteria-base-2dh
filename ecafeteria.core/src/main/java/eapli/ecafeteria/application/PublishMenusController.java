/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenusRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author tiagogoncalves
 */
public class PublishMenusController implements Controller{
    
    public Iterable<Menu> listMenus() throws DataIntegrityViolationException {
         ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        
        final MenusRepository repo = PersistenceContext.repositories().menus();
        
        return repo.unpublishedMenus();
    
    }
    
    public boolean publishMenu(Menu m)throws DataIntegrityViolationException{
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        
        final MenusRepository repo = PersistenceContext.repositories().menus();
        
        if(m.publish()){
            if(repo.save(m)!=null){
                return true;
            }   
        }
        
        return false;
    }
    
}
