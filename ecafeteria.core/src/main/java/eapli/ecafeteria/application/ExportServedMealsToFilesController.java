package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.files.FileExporterFactory;
import eapli.ecafeteria.persistence.jpa.JpaReservationRepository;
import eapli.framework.application.Controller;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Pedro Abreu
 */
public class ExportServedMealsToFilesController implements Controller {

    public ExportServedMealsToFilesController() {
        
    }
    /**
     * returns a list of reservations given a time period
     * @param start start date
     * @param end end date
     * @return list of reservation
     */
    public List<Reservation> setTimePeriod(Date start,Date end) {
        JpaReservationRepository repo = (JpaReservationRepository) PersistenceContext.repositories().reservations();
        return repo.getReservationsByTimePeriod(start,end);
        
    }
    
    /**
     * exports the lsit of reservations to specific file
     * @param option the option that commands what file to write to
     * @param reservation the list of reservations
     */
    public void exportToFile(int option,List<Reservation> reservation) {
       FileExporterFactory fec = new FileExporterFactory();
       fec.getStrategy(option, reservation);
    }
}
