/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Price;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Vasco
 */
public class RegisterDishController implements Controller {

    public Dish registerDish(String name, DishType dishType, long kcal, int fatPer, Price price) throws DataIntegrityViolationException {
         ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        
        final Dish newDish = new Dish(name, kcal, fatPer, price, dishType);
        final DishRepository repo = PersistenceContext.repositories().dish();
        
        repo.add(newDish);
        return newDish;
    }
    
    public Iterable<DishType> listDishTypes() {
        return new ListDishTypeService().allDishTypes();
    }

}
