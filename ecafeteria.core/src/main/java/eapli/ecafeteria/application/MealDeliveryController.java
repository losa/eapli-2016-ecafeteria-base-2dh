/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.mealbooking.Delivered;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.ReservMealRepository;
import eapli.ecafeteria.persistence.ReservationRepository;
import eapli.framework.application.Controller;
import java.util.Date;

/**
 *
 * @author Tiago
 */
public class MealDeliveryController implements Controller {
    
    RepositoryFactory repoFactory;
    
    ReservMealRepository reservationRepo;

    public MealDeliveryController() {
        this.repoFactory = PersistenceContext.repositories();
        
        this.reservationRepo = repoFactory.reservmeals();
    }
    
    public boolean deliverReservation(String userID) {
        
        POS pos = AppSettings.instance().session().getPOS();
        
        Date dateToWork = pos.DateToWork();
        MealType mealToWork = pos.MealToWork();
                
        Reservation reservation = this.reservationRepo.getReservation(userID, dateToWork, mealToWork);
        
        reservation.setState(new Delivered());
        
        reservationRepo.save(reservation);
        
        return true;
    }
    
    
    
}
