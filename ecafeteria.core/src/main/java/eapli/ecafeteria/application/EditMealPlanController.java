/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.List;

/**
 *
 * @author Bruno Stamm
 */
public class EditMealPlanController {

    // meal plan that will be changed
    private MealPlan meal_plan;

    /**
     * Returns a list of possible meal plans to edit.
     * @return list of meal plans.
     */
    public List<MealPlan> possibleMealPlans() {
        final MealPlanRepository mealPlanRepo = PersistenceContext.repositories().mealPlanning();
        return mealPlanRepo.editableMealPlans();
    }
    
    /**
     * Sets the meal plan to change.
     *
     * @param mealPlan meal plan.
     */
    public void selectMealPlan(MealPlan mealPlan) {
        this.meal_plan = mealPlan;
    }

    /**
     * Sets a new cooked quantity of the meal plan item received.
     *
     * @param planItem meal plan item.
     * @param quantity new quantity.
     */
    public void changeQuantity(MealPlanItem planItem, int quantity) {
        meal_plan.changeQuantity(planItem, quantity);
    }
    
    public void save() throws DataIntegrityViolationException{
        final MealPlanRepository mealPlanRepo = PersistenceContext.repositories().mealPlanning();
        mealPlanRepo.save(meal_plan);
    }

}
