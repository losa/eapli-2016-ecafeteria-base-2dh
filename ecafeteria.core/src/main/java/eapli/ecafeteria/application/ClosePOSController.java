
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.cafeteria.POSState;
import eapli.ecafeteria.domain.mealbooking.Active;
import eapli.ecafeteria.domain.mealbooking.Delivered;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.mealbooking.Undelivered;
import eapli.ecafeteria.persistence.POSRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.ReservMealRepository;
import eapli.framework.application.Controller;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiago
 */
public class ClosePOSController implements Controller{
    
    //repository factory
    RepositoryFactory repoFactory;
    //POS repository
    POSRepository posRepo;
    //Reservations repository
    ReservMealRepository reservationRepository;
    
    POS pos;
    
    /**
     * controller construtor
     */
    public ClosePOSController() {
        Session session = AppSettings.instance().session();
        
        pos = session.getPOS();
        
        this.repoFactory = PersistenceContext.repositories();
        
        this.posRepo = repoFactory.pos();
        this.reservationRepository = repoFactory.reservmeals();
    }
    
    public List<Reservation> deliveredMeals(){
        List<Reservation> deliveredMeals = new ArrayList<>();
        
        for (Reservation reservation : reservationRepository.getReservations(pos.DateToWork(), pos.MealToWork()))
            if(reservation.getState() instanceof Delivered)
                deliveredMeals.add(reservation);
        
        return deliveredMeals;
    }
    
    public POS closePos() {        
        this.finishReservations();
        
        this.pos.setState(POSState.CLOSED);
        
        this.posRepo.save(this.pos);        
        
        Session session = AppSettings.instance().session();
        
        session.closePOS();
        
        return pos;
        
    }
    
    private void finishReservations() {
        for (Reservation reservation : reservationRepository.getReservations(pos.DateToWork(), pos.MealToWork())) {
            if(reservation.getState() instanceof Active){
                reservation.setState(new Undelivered());
                this.reservationRepository.save(reservation);
            }
        }
    }
}

