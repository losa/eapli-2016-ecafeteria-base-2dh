package eapli.ecafeteria.application;


import eapli.ecafeteria.AppSettings;
import eapli.framework.application.Controller;
/**
 *
 * @author josepedromoreira
 */
public class LogoutController implements Controller  {

    public void logout() {
        AppSettings.instance().removeSession();
    }
     
}
