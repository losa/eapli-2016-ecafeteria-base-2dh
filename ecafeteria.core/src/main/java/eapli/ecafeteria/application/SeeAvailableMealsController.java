/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.ReservationRepository;
import eapli.framework.application.Controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author tiago Frois
 */
public class SeeAvailableMealsController extends Observable implements Controller ,Observer{

    RepositoryFactory repoFactory;
    
    MealRepository mealRepo;
    private MealType mealType;
    private Date day;
    private POS pos;

    public SeeAvailableMealsController() {
        this.repoFactory = PersistenceContext.repositories();
        
        this.mealRepo = repoFactory.meals();
        this.day = eapli.ecafeteria.domain.authz.Session.getPOS().DateToWork();
        mealType = eapli.ecafeteria.domain.authz.Session.getPOS().MealToWork();
    }
    /**
     * Returns the AvailableMeals in a specific part of the day
     * @return 
     */
    public List<MealPlanItem> seeAvailableMeals(){
        List<MealPlanItem> availableMeals =  new ArrayList();
        
        availableMeals = this.mealRepo.getReservation(this.day, this.mealType);
        for (MealPlanItem availableMeal : availableMeals) {
            availableMeal.addObserver(this);
        }
        return availableMeals;
    }

    @Override
    public void update(Observable o, Object o1) {
        setChanged();
        notifyObservers(o1);
    }
}
