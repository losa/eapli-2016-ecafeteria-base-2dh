/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.framework.application.Controller;

/**
 *
 * @author tiago frois
 */
public class ChangePasswordController implements Controller{
    RepositoryFactory repoFactory;
    UserRepository suRepo;
    
    public ChangePasswordController() {
        this.repoFactory = PersistenceContext.repositories();
        this.suRepo = this.repoFactory.users();
    }
    
    public void changePassword(String password){
        SystemUser su = AppSettings.instance().session().getUser();
        su.changePassword(password);
        this.suRepo.save(su);
    }
    
}
