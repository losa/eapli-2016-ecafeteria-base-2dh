/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenusRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.domain.TimePeriod2;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Calendar;


/**
 *
 * @author JoséDiogo
 */
public class CreateMenusController implements Controller {

    public Menu createMenu(Calendar begin, Calendar end, String desc) throws DataIntegrityViolationException, IllegalArgumentException{
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        
        final TimePeriod2 period = new TimePeriod2(begin, end);
        
        final Menu menu = new Menu(period,desc);        
        final MenusRepository repo = PersistenceContext.repositories().menus();
        
        repo.add(menu);
        return menu;
    }
}
