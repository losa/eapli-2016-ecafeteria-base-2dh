/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenusRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tiagogoncalves
 */
public class EditMenusController implements Controller {
    
    private Menu activeMenu;
    
    private ArrayList<Meal> newMeals = new ArrayList<>();

    public Iterable<Menu> listMenus() throws DataIntegrityViolationException {
         ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        
        final MenusRepository repo = PersistenceContext.repositories().menus();
        
        return repo.unpublishedMenus();
    
    }

    public void changeMenu(Menu theMenu) {
        this.activeMenu = theMenu;
    }

    public void newMeal(MealType mealType, Dish dish, Calendar date, Menu theMenu) throws IllegalArgumentException {
       Meal m = new Meal(date, mealType, dish, theMenu);
       if(m != null){
           newMeals.add(m);
       }
    }

    public void saveNewMeals(){
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        
        final MealRepository repo = PersistenceContext.repositories().meals();
        
        for(Meal m: this.newMeals){
            try {
                repo.add(m);
            } catch (DataIntegrityViolationException ex) {
                Logger.getLogger(EditMenusController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }


    public Iterable<Dish> listDishes() {
         ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        
        final DishRepository repo = PersistenceContext.repositories().dish();
        
        return repo.all();
    }



}

