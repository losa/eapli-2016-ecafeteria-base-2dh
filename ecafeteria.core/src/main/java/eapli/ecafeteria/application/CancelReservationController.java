/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.Balance;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.Canceled;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.mealbooking.ReservationState;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Price;
import eapli.ecafeteria.persistence.jpa.JpaCancelReservationRepository;
import eapli.framework.application.Controller;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Nuno
 */
public class CancelReservationController implements Controller{
    
    private Session s;
    
    private JpaCancelReservationRepository repo;
    
    private SystemUser su;
    
    private CafeteriaUser cu;
    
    private Reservation r;
    
    private Meal m;
    
    private Price p;
    
    private Account a;
    
    private Balance b;
    
    public CancelReservationController() {
        repo = new JpaCancelReservationRepository();
    }
    
    public ArrayList<Reservation> getReservationList() {
        su = s.authenticatedUser();
        Username id = su.id();
        cu = repo.getCafeteriaUserByID(id);
        ArrayList<Reservation> rl = repo.getReservationList(cu);
        return rl;
    }
    
    public void setReservation(Reservation r) {
        this.r = r;
    }
    
    public void setReservationState() {
        ReservationState state = new Canceled();
        repo.setReservationState(r);
        r.setState(state);
    }
    
    public List<Meal> getAvailableMeals() {
        return repo.getAvailableMeals(new Date());
    }
    
    public void addBalance() {
        m = r.getMeal();
        p = m.getPrice();
        double v = p.getValue();
        Calendar d = Calendar.getInstance();
        Calendar d2 = m.getDate();
        MealType mt = m.getMealType();
        a = cu.getAccount();
        b = a.getBalance();
        if (d.get(Calendar.YEAR) < d2.get(Calendar.YEAR)) {
            b.deposit(v);
            a.addMovementCancel(v);
        } else if (d.get(Calendar.YEAR) == d2.get(Calendar.YEAR)) {
            if (d.get(Calendar.MONTH) < d2.get(Calendar.MONTH)) {
                b.deposit(v);
                a.addMovementCancel(v);
            } else if(d.get(Calendar.MONTH) == d2.get(Calendar.MONTH)) {
                if (d.get(Calendar.DAY_OF_MONTH) < d2.get(Calendar.DAY_OF_MONTH)) {
                    b.deposit(v);
                    a.addMovementCancel(v);
                } else if (d.get(Calendar.DAY_OF_MONTH) == d2.get(Calendar.DAY_OF_MONTH)) {
                    if (mt == MealType.LUNCH) {
                        if (d.get(Calendar.HOUR_OF_DAY) < 10) {
                            b.deposit(v);
                            a.addMovementCancel(v);
                        } else {
                            b.deposit((v/2));
                            a.addMovementCancel(v/2);
                        }
                    }
                    if (mt == MealType.DINNER) {
                        if (d.get(Calendar.HOUR_OF_DAY) < 16) {
                            b.deposit(v);
                            a.addMovementCancel(v);
                        } else {
                            b.deposit((v/2));
                            a.addMovementCancel(v/2);
                        }
                    }
                }
                else {
                    System.out.println("The meal already happened!");
                }
            }
            else {
                System.out.println("The meal already happened!");
            }
        }
        else {
            System.out.println("The meal already happened!");
        }
    }
    
    
    
}
