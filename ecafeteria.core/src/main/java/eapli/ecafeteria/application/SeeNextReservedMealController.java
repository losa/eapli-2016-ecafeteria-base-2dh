/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.ReservationRepository;
import eapli.framework.application.Controller;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.Persistence;

/**
 *
 * @author Gisela Osório
 */
public class SeeNextReservedMealController implements Controller {

    private ReservationRepository repository;
    private SystemUser user;
    private Username id;

    public SeeNextReservedMealController(){
        
    }
    
    public Meal getNextReservedMeal() {
        user = Session.authenticatedUser();
        id = user.id();
        repository = PersistenceContext.repositories().reservations();
        List<Reservation> listaReservas = repository.getAllReservations(id);
        for (int i = 0; i < listaReservas.size() - 1; i++) {
            for (int j = i + 1; j < listaReservas.size(); j++) {
                if (listaReservas.get(i).getMeal().getDate().after(listaReservas.get(j).getMeal().getDate())) {
                    Reservation reserva;
                    reserva = listaReservas.get(i);
                    listaReservas.set(i, listaReservas.get(j));
                    listaReservas.set(j, reserva);

                }
            }
        }
        Calendar dataAtual = DateTime.now();
        for (Reservation r : listaReservas) {
            if (r.getMeal().getDate().after(dataAtual)) {
                return r.getMeal();
            }
        }
        return null;
    }
}
