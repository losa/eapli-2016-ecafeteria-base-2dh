/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.cafeteria.consoleapp.presentation.authz;

import eapli.ecafeteria.application.ChangePasswordController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

/**
 *
 * @author tiago frois
 */
public class ChangePasswordUI extends AbstractUI{
    
    private final ChangePasswordController theController = new ChangePasswordController();
    
    
    protected Controller controller() {
        return this.theController;
    }
    @Override
    protected boolean doShow() {
        System.out.println("Input new Password:");
        final String password = Console.readLine("Password:");

        this.theController.changePassword(password);
        System.out.println("Password Changed with sucess!");
        return true;

    }

    @Override
    public String headline() {
       return "Change Password";
    }
    
}
