package eapli.cafeteria.consoleapp.presentation.authz;

import eapli.ecafeteria.application.LogoutController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;


/**
 *
 * @author josep
 */
public class LogoutUI extends AbstractUI {
     private final LogoutController theController = new LogoutController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        this.theController.logout();
        System.out.println("Logout Successful");
        return false;
    }

    @Override
    public String headline() {
        return "Logout";
    }
    
}
