/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.cafeteria.consoleapp.Meals;

import eapli.ecafeteria.application.CancelReservationController;
import eapli.ecafeteria.application.CheckAccountBalanceController;
import eapli.ecafeteria.application.SeeNextReservedMealController;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Nuno
 */
public class CancelReservUI extends AbstractUI{
   
    private final CancelReservationController controller = new CancelReservationController();
    
    protected Controller controller() {
        return this.controller;
    }
    
    protected boolean doShow() {
        
        List<Meal> meals = controller.getAvailableMeals();
        if(meals.isEmpty()){
            System.out.println("No available meals!");
            return false;
        }
        
        
        Scanner in = new Scanner(System.in);
        int cont=1;
        
        CheckAccountBalanceController saldoController = new CheckAccountBalanceController();
        SeeNextReservedMealController nextMeal = new SeeNextReservedMealController();
        
        System.out.println("Current Balance: " + saldoController.getCurrentBalance() + "\n");
        System.out.println("Next Reserved Meal: " + nextMeal.getNextReservedMeal().toString() + "\n");
        
        System.out.println("Cancel Reservation: ");
        ArrayList<Reservation> listaReservas = controller.getReservationList();

        for (Reservation r : listaReservas) {
            System.out.println((listaReservas.indexOf(r) + 1) + ". Meal: " + r.getMeal().getInfoMeal());
            cont++;
        }

        System.out.println("Insert an option: ");

        int opt = in.nextInt();
        
        while(opt < 1 || opt > cont){
            System.out.println("Invalid Option! Insert an option: ");
            opt = in.nextInt();
        }
        
        Reservation r = listaReservas.get(opt - 1);

        controller.setReservation(r);

        controller.setReservationState();

        controller.addBalance();
        
        return true;
    }

    @Override
    public String headline() {
        return "Cancel Reservation";
    }
}
