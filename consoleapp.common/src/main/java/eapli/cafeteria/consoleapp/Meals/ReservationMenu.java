/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.cafeteria.consoleapp.Meals;

import eapli.framework.actions.ReturnAction;
import eapli.framework.actions.ShowMessageAction;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;

/**
 *
 * @author JoaoPedro
 */
public class ReservationMenu extends Menu{
    
     private static final int EXIT_OPTION = 0;
     private static final int RESERV_MEAL_OPTION = 1;
     private static final int CANCEL_RESERVATION_OPTION = 2;
     private static final int CHECK_RESERVATIONS_OPTION = 3;
     
     public ReservationMenu() {
        super("Reservation >");
        buildReservationMenu();
    }

    private void buildReservationMenu() {
        add(new MenuItem(RESERV_MEAL_OPTION, "Reserv Meal", new ReservMealAction()));
        add(new MenuItem(CANCEL_RESERVATION_OPTION, "Cancel Reservation", new CancelReservAction()));
        add(new MenuItem(CHECK_RESERVATIONS_OPTION, "Check Reservations", new ShowMessageAction("Not implemented yet")));
        add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
    }
     
}
