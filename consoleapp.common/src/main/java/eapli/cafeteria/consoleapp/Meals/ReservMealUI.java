/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.cafeteria.consoleapp.Meals;

import eapli.ecafeteria.application.ReservMealController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Price;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author JoaoPedro
 */
public class ReservMealUI extends AbstractUI implements Observer{
    private final ReservMealController theController = new ReservMealController();
    /*
     * private final Logger logger = LoggerFactory.getLogger(LoginUI.class);
     * 
     * public Logger getLogger() { return logger; }
     */

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        List<Meal> meals = theController.getAvailableMeals();
        if(meals.isEmpty()){
            System.out.println("No available meals!");
            return false;
        }
        System.out.println("Available Meals");
        for(int i = 0; i< meals.size(); i++){
            System.out.println(i+". "+meals.get(i).description());
        }
        int opcao = 0;
        int meal = -1;
        List<Object> info;
        while(opcao != 1){
            meal = Console.readInteger("Choose the number of the desired meal:");
            while(meal < 0 || meal > meals.size()){
                meal = Console.readInteger("Please insert a valid number:");
            }
            info = theController.getInfo(meals.get(meal));
            System.out.println("Info Meal:");
            System.out.println("Price : "+((Price)info.get(0)).getValue()+"€");
            System.out.println("Meal Type : "+((MealType)info.get(1)).toString());
            System.out.println("Dish : "+ ((Dish)info.get(2)).id());
            System.out.println("Date : "+ ((Calendar)info.get(3)).getTime());
            opcao = Console.readInteger("Continue?(1-yes, 0-no)");
        }

        theController.registReservation(meals.get(meal),this);
        return true;
    }

    @Override
    public String headline() {
        return "Reserve Meal";
    }
    
     @Override
    public void update(Observable o, Object arg) {
        System.out.println(arg + "Alert!");
    }
}
