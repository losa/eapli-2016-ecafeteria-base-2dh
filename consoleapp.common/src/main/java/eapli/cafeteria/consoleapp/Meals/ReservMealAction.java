/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.cafeteria.consoleapp.Meals;

import eapli.framework.actions.Action;

/**
 *
 * @author JoaoPedro
 */
public class ReservMealAction implements Action{

    @Override
    public boolean execute() {
        return new ReservMealUI().show();
    }
    
}
