package eapli.framework.domain;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class TimePeriodTest {
    
    private TimePeriod week;
    
    public TimePeriodTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
                
        Date start = new Date(2016, 5, 15);
        Date end = new Date(2016, 5, 21);
        
        week = new TimePeriod(DateTime.dateToCalendar(start),DateTime.dateToCalendar(end));
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of includesDate method, of class TimePeriod.
     */
    @Test
    public void testIncludesDateFromWeek() {
        System.out.println("includesDateFromWeek");

        Date between = new Date(2016, 5, 17);
        
        Calendar date = DateTime.dateToCalendar(between);

        assertTrue(week.includesDate(date));
    }
    
    
    /**
     * Test of includesDate method, of class TimePeriod.
     */
    @Test
    public void testIncludesDateNotFromWeek() {
        System.out.println("includesDateNotFromWeek");
        
        Date between = new Date(2016, 5, 1);
        
        Calendar date = DateTime.dateToCalendar(between);
        
        assertFalse(week.includesDate(date));
    }

}
