package eapli.framework.domain;

import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paulo Gandra Sousa
 *
 */
@Embeddable
@XmlRootElement(name = "period")
public class TimePeriod2 {

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.DATE)
    @XmlElement(name = "start")
    Calendar start;

    @Temporal(TemporalType.DATE)
    @XmlElement(name = "end")
    Calendar end;

    /**
     * Constructor
     *
     * @param start
     * @param end
     */
    public TimePeriod2(Calendar start, Calendar end) {
        this.start = start;
        this.end = end;
    }

    protected TimePeriod2() {
    }

    public Calendar start() {
        return this.start;
    }

    public Calendar end() {
        return this.start;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TimePeriod2 other = (TimePeriod2) obj;
        return this.start.equals(other.start)
                && this.end.equals(other.end);
    }
    
    @Override
    public String toString(){
        return "Period: "+this.start.getTime().toString()+" - "+this.end.getTime().toString();
    }

}
