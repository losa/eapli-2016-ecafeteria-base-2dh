/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register;

import eapli.ecafeteria.application.MealDeliveryController;
import eapli.ecafeteria.application.OpenPOSController;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 *
 * @author Tiago
 */
public class OpenPosUI extends AbstractUI {
    private final OpenPOSController theController = new OpenPOSController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        Date data = new Date();
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(data);   // assigns calendar to given date 
        int hour = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        int day =  calendar.get(Calendar.DAY_OF_MONTH);       // gets month number, NOTE this is zero based!
        MealType type;
        
           if(hour < 15){
            type = MealType.LUNCH;
            
        }else{
            type = MealType.DINNER;
        }
        System.out.println("Open POS in this period?");
        System.out.println("Day: "+day);
        System.out.println("Meal Type: "+ type);

        System.out.println("1. Yes");
        System.out.println("2. Choose other time.");
        int option = Console.readOption(1, 2, 0); 
        
        if(option == 1){
            //Omisson TIME
            
        }else{
            System.out.println("Select day to work: (DD-MM-YYYY)");
            Scanner ler = new Scanner(System.in);
            String vec[] = ler.next().split("-");
            day = Integer.parseInt(vec[0]);
            int month =Integer.parseInt(vec[1]);
            int year = Integer.parseInt(vec[2]);
            calendar.set(year, month-1, day);

            data = calendar.getTime();
            System.out.println("Select meal to work: ");
            System.out.println("1. Lunch");
            System.out.println("2. Dinner");

            option = Console.readOption(1, 2, 0);

            if(option == 1){
                type = MealType.LUNCH;

            }else{
                type = MealType.DINNER;
            }
        }
        
         theController.choosePOSToWork(data, type);
         
        return false;
    }

    @Override
    public String headline() {
        return "Open POS";
    }
}