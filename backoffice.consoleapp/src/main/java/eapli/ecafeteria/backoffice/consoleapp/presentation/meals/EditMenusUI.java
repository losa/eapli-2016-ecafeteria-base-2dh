/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.EditMenusController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;

import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;
import java.util.ArrayList;
import java.util.Date;
/**
 *
 * @author JoséDiogo
 */
class EditMenusUI extends AbstractUI{
    
    private final EditMenusController theController = new EditMenusController();
    private ArrayList<Meal> newMeals = new ArrayList<>();
    
    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        
        final Iterable<Menu> menus;
        try {
                menus = this.theController.listMenus();

            final SelectWidget<Menu> selector = new SelectWidget<>(menus, new MenusPrinter());
            selector.show();
            final Menu theMenu = selector.selectedElement();
            if (theMenu != null) {
               this.theController.changeMenu(theMenu);
               
               while(Console.readBoolean("Insert meal (y/n)?")){
                    
                   try{
                       
                    Dish d = selectDish();
                    if(d != null){
                        theController.newMeal(selectMealType(), d
                            , Console.readCalendar("Insert a date for the meal"), theMenu);
                    }else{
                        return false;
                    }
                    
                   }catch(IllegalArgumentException ex){
                       System.out.println("Invalid arguments. Meal not created");
                   }
               }
               
               theController.saveNewMeals();
                
            }
        
        } catch (DataIntegrityViolationException ex) {
           
            
            //Logger.getLogger(EditMenusUI.class.getName()).log(Level.SEVERE, null, ex);
          
        }
        return false;
    }

    @Override
    public String headline() {
        return "Edit Menu";
    }

    private MealType selectMealType() {
            
        MealType mt[] = MealType.values();
        
        int i=0;
        for(MealType c : mt){
            System.out.print(i+" - "+c+"\n");
            i++;
        }
        
        int index = Console.readInteger("Select a meal type");
        
        return mt[index] ;
    }

    private Dish selectDish() throws DataIntegrityViolationException {
        
        System.out.println("Dishes:");
        final Iterable<Dish> allDishes = this.theController.listDishes();
        
        if(!allDishes.iterator().hasNext()){
            System.out.println("No dishes registered.");
            return null;
        }else{
            final SelectWidget<Dish> selector = new SelectWidget<>(allDishes, new DishPrinter());
            selector.show();
            final Dish updtDish = selector.selectedElement();
            
            return updtDish;
        }   
    }    
}