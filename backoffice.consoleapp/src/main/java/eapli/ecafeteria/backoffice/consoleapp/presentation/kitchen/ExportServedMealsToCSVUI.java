package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.ExportServedMealsToFilesController;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pedro Abreu
 */
public class ExportServedMealsToCSVUI extends AbstractUI {

    private ExportServedMealsToFilesController theController = new ExportServedMealsToFilesController();

    private static final String HEADLINE = "Export Served Meals To .csv";

    private static final int CSV_OPTION = 1;

    @Override
    protected boolean doShow() {
        System.out.println("Enter a time period");
        Date begin = Console.readDate("From (Date in dd-mm-yyyy)", "dd-MM-yyyy");
        Date end = Console.readDate("To (Date in dd-mm-yyyy)", "dd-MM-yyyy");
        List<Reservation> reservation = theController.setTimePeriod(begin, end);
        theController.exportToFile(CSV_OPTION, reservation);
        System.out.println("Exported " + reservation.size() + " reservations to csv");
        try {
            URI fileloc = new URI("Reservation.csv");
            Desktop.getDesktop().browse(fileloc);
        } catch (URISyntaxException ex) {
            System.out.println("could not find the file");
        } catch (IOException ex) {
            System.out.println("could not open the file");
        }
        return true;
    }

    @Override
    public String headline() {
        return HEADLINE;
    }

}
