/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.ShowReservationController;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Pedro Abreu
 */
public class ShowReservationUI extends AbstractUI {

    private ShowReservationController theController = new ShowReservationController();

    private static final String HEADLINE = "Check Reservations";

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        int option;
        int i = 0;
        int selectedItem;
        List<DishType> dishTypes = theController.getDishTypes();
        MealType[] mealType = theController.getMealTypes();
        List<Dish> dish = theController.getDishes();
        System.out.println("Reservations by?\n");
        System.out.println("1:Dish");
        System.out.println("2:Dish Types");
        System.out.println("3:Meal Types");
        System.out.println("4:Date");
        System.out.println("0:Exit");

        option = Console.readOption(1, 4, 0);
        if (option == 1) {
            i = 1;
            for (Dish d : dish) {
                System.out.println(i + ": " + d.id());
                i++;
            }
            System.out.println("0:Exit");

            selectedItem = Console.readOption(1, dish.size(),0);
            if(selectedItem == 0) {
                return false;
            }
            
            selectedItem--;
            Dish d = dish.get(selectedItem);
            List<Reservation> result = theController.selectDish(d);
            if (result.isEmpty() || result == null) {
                System.out.println("No results");
                return false;
            } else {
                System.out.println("There are " + result.size() + " reservations with this dish");
                for (Reservation r : result) {
                    System.out.println(r.toString());
                }
                return true;
            }

        } else if (option == 2) {
            i = 1;
            for (DishType dt : dishTypes) {
                System.out.println(i + ": " + dt.id());
                i++;
            }

            System.out.println("0:Exit");

            selectedItem = Console.readOption(1, dishTypes.size(),0);
            if(selectedItem == 0) {
                return false;
            }
            
            selectedItem--;
            DishType dt = dishTypes.get(selectedItem);
            List<Reservation> result = theController.selectDishTypes(dt);
            if (result.isEmpty() || result == null) {
                System.out.println("No results");
                return false;
            } else {
                System.out.println("There are " + result.size() + " reservations with this dish type");
                for (Reservation r : result) {
                    System.out.println(r.toString());
                }
                return true;
            }

        } else if (option == 3) {
            i = 1;
            for (MealType m : mealType) {
                System.out.println(i + ":" + m);
                i++;
            }
            System.out.println("0:Exit");

            selectedItem = Console.readOption(1, mealType.length,0);
            if(selectedItem == 0) {
                return false;
            }
            
            selectedItem--;
            List<Reservation> result = theController.selectMealTypes(mealType[selectedItem]);
            if (result.isEmpty() || result == null) {
                System.out.println("No results");
                return false;
            } else {
                System.out.println("There are " + result.size() + " reservations with this meal type");
                for (Reservation r : result) {
                    System.out.println(r.toString());
                }
                return true;
            }
        } else if (option == 4) {
            Date date = Console.readDate("Date in dd-mm-yyyy","dd-MM-yyyy");
            List<Reservation> result = theController.selectDate(date);
            if (result.isEmpty() || result == null) {
                System.out.println("No results");
                return false;
            } else {
                System.out.println("There are " + result.size() + " reservations with this date");
                for (Reservation r : result) {
                    System.out.println(r.toString());
                }
                return true;
            }

        } else if (option == 0) {
            System.out.println("Back to kitchen main menu");
            return true;
        } else {
            System.out.println("Invalid option, try again");
            return false;
        }

    }

    @Override
    public String headline() {
        return HEADLINE;
    }

}
