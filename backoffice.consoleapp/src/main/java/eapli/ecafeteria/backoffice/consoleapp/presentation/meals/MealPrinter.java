package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.visitor.Visitor;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author Marcelo Oliveira
 */
public class MealPrinter implements Visitor<Meal> {

    @Override
    public void visit(Meal visitee) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        System.out.printf("%-15s | %-30s | %-5s\n", dateFormat.format(visitee.getDate().getTime()),visitee.description(), String.valueOf(visitee.getMealType()));
    }

    @Override
    public void beforeVisiting(Meal visitee) {
        // nothing to do
    }

    @Override
    public void afterVisiting(Meal visitee) {
        // nothing to do
    }
}
