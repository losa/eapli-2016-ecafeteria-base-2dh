/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register;

import eapli.ecafeteria.application.AddFundsController;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import java.util.Scanner;

/**
 *
 * @author Tiago
 */
public class AddFundsUI extends AbstractUI {

    private final AddFundsController theController = new AddFundsController();
    
    //Cafeteria User
    CafeteriaUser cafUser;

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        int tentativas, carregaCount;
        
        System.out.println("Insira o seu número mecanografico: ");
        Scanner read = new Scanner(System.in);
        // permite 3 tentativas
        for (tentativas = 0; tentativas < 3; tentativas++) {
            
           if( theController.getUserById(read.next()))
               break;
            
            System.out.println("Número mecanografico não existe! Insira novamente o seu número (tentativas -> "+(tentativas+1)+")");
        }
        if(tentativas == 3){
            System.out.println("Atingido limite máximo de tentativas.");
            return false;
        }
        
        System.out.println("Insira o valor a carregar: ");
        read = new Scanner(System.in);
        double valor=read.nextDouble();
        
        // permite inserir 3 vezes o montante
        for (carregaCount = 0; carregaCount < 3; carregaCount++) {
            
           if( valor > 0)
               break;
            
            System.out.println("Valor de carregamento inválido! Insira novamente montante a carregar (tentativas -> "+(carregaCount+1)+")");
        }
        if(carregaCount == 3){
            System.out.println("Não inseriu montante de carregamento válido. Volte mais tarde!");
            return false;
        }
        
        theController.addFunds(valor);
        System.out.println("Montante actual -> " +this.theController.cafUser.getAccount().getBalance().getValue());

        return true;
    }

    @Override
    public String headline() {
        return "Meal Delivery";
    }
}