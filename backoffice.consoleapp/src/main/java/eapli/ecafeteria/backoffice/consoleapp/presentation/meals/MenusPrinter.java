/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author tiagogoncalves
 */
public class MenusPrinter  implements Visitor<Menu> {

    @Override
    public void visit(Menu visitee) {
        System.out.printf("%-30s | Published: %-5s | Description: %s-30\n", visitee.period().toString(), visitee.isPublished(), visitee.description());
    }

    @Override
    public void beforeVisiting(Menu visitee) {
        // nothing to do
    }

    @Override
    public void afterVisiting(Menu visitee) {
        // nothing to do
    }
    
}
