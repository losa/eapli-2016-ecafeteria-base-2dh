/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.CreateMealPlanController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Bruno Stamm
 */
public class CreateMealPlanUI extends AbstractUI {
    
    private final CreateMealPlanController theController = new CreateMealPlanController();
    
    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        
        try {
            // asks week
            Calendar beginning = Console.readCalendar("Week beginning date (dd-mm-yyyy): ");
            Calendar ending = Console.readCalendar("Week ending date (dd-mm-yyyy): ");
            // FIXME error checking if there is already a meal plan for that period
            
            final List<Meal> mealList = theController.selectWeek(beginning, ending);
            System.out.println("");
            
            if(mealList.isEmpty()) {
                System.out.println("No published Menus for this period\n");
            } else {
                System.out.println(".....................................");
                for(Meal meal : mealList) {
                    System.out.println(meal);
                    System.out.println("");
                    int quantity = Console.readInteger("Quantity planned: ");

                    theController.newMealQuantity(meal, quantity);

                    System.out.println("");
                }
                System.out.println(".....................................");
                System.out.println("");

                if(Console.readBoolean("Save Meal Plan? (y/n)")) {
                    theController.save();
                    System.out.println("\nSuccess");
                }
            }
            
        }catch (IllegalArgumentException exeption) {
            System.out.println("Invalid info!\n");
        }catch(DataIntegrityViolationException e) {
            System.out.println("Already existing meal plan!\n");
        }
        
        return false;
    }

    @Override
    public String headline() {
        return "Create Meal Plan";
    }
    
}
