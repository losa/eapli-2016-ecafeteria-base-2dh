/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register;

import eapli.ecafeteria.application.SeeAvailableMealsController;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.List;
import java.util.Map;
import java.util.Observer;
import java.util.Scanner;
import javafx.beans.Observable;

/**
 *
 * @author Tiago
 */
public class SeeAvailableMealsUI extends AbstractUI implements Observer{

    private final SeeAvailableMealsController theController = new SeeAvailableMealsController();
    
    CafeteriaUser cafUser;

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        theController.addObserver(this);
        int i=0;
        System.out.println("--------Available Meals ---------");
        List<MealPlanItem> meals = theController.seeAvailableMeals();
        for (MealPlanItem meal : meals) {
            System.out.println(i+". "+meal.meal().description() +" quantity:"+meal.AvailableMeals());
            i++;
        }

        return true;
    }

    @Override
    public String headline() {
        return "Meal Delivery";
    }

    @Override
    public void update(java.util.Observable o, Object o1) {
        System.out.println(o1);
    }
}