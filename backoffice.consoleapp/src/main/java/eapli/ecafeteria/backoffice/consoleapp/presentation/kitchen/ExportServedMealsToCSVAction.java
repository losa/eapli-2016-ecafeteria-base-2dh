package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.framework.actions.Action;

/**
 *
 * @author Pedro Abreu
 */
public class ExportServedMealsToCSVAction implements Action {
  @Override
    public boolean execute() {
        return new ExportServedMealsToCSVUI().doShow();
    }    

}
