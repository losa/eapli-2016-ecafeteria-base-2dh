/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register;

import eapli.ecafeteria.application.ClosePOSController;
import eapli.ecafeteria.application.OpenPOSController;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Tiago
 */
public class ClosePosUI extends AbstractUI {
    private final ClosePOSController theController = new ClosePOSController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        List<Reservation> reservas = theController.deliveredMeals();
         
        if(reservas.isEmpty())
            System.out.println("No Last Minute Meals sold!");
        else
            for (Reservation reserva : reservas) {
                System.out.println(reserva.getMeal() + " " + reserva.getUser().getMecanographicNumber());
            }
        
        theController.closePos();
        
        return false;
    }

    @Override
    public String headline() {
        return "Close POS";
    }
}