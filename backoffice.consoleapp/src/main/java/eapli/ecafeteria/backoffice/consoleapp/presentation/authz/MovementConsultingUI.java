/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.authz;

import eapli.ecafeteria.application.MovementConsultingController;
import eapli.ecafeteria.domain.mealbooking.Movement;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author José Sousa
 */
public class MovementConsultingUI {
    
    
    private MovementConsultingController controller;
    
    public MovementConsultingUI () {
        
        controller = new MovementConsultingController();
        
        Scanner in = new Scanner(System.in);
        
        System.out.println("Consulta de Movimentos de Conta:");
        
        System.out.println("\tInsira uma data a partir da qual queira ver os movimentos de conta (formato AAAA-MM-DD): ");
        String auxDate = in.nextLine();
        String [] date = auxDate.split("-");
        
        int year = Integer.parseInt(date[0]);
        int month = Integer.parseInt(date[1]);
        int day = Integer.parseInt(date[2]);
        
        controller.setDate(year, month, day);
        
        System.out.println("\tMovimentos a partir da data inserida: ");
        List<Movement> movementsList = controller.getMovementListByDate();
        
        for (Movement m : movementsList) {
            System.out.println("\t\t" + movementsList.indexOf(m) + ". " + m.getDescription() + " - " + m.getDate() + " - " + m.getMovement() + "€");
        }
        
    }
}
