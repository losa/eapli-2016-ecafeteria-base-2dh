package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Syd
 */
public class MealPlanItemDifferencePrinter implements Visitor<MealPlanItem> {

    @Override
    public void visit(MealPlanItem visitee) {
        System.out.printf("%-35s | %-10s | %-10s | %-10s\n", visitee.meal().description(),visitee.quantityCooked(), visitee.differenceCookedUnsold(), visitee.differencePrediction());
    }

    @Override
    public void beforeVisiting(MealPlanItem visitee) {
        // nothing to do
    }

    @Override
    public void afterVisiting(MealPlanItem visitee) {
        // nothing to do
    }
}
