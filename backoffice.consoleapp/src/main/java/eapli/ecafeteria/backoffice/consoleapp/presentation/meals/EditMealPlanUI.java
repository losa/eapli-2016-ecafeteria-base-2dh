/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.EditMealPlanController;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;
import java.util.List;

/**
 *
 * @author Bruno Stamm
 */
public class EditMealPlanUI extends AbstractUI{

    private final EditMealPlanController theController = new EditMealPlanController();
    
    @Override
    protected boolean doShow() {
        
        try {
            System.out.println("Meal Plans:\n");
            
            // get meal plans
            final List<MealPlan> mealPlanList = theController.possibleMealPlans();
            
            // if there are no meal plans
            if(mealPlanList.isEmpty()) {
                System.out.println("There is no Meal Plan published");
            } else {
                // select meal plan
                final SelectWidget<MealPlan> plansSelector = new SelectWidget<>(mealPlanList, new MealPlanPrinter());
                plansSelector.show();
                final MealPlan mealPlan = plansSelector.selectedElement();
                
                if(mealPlan != null) {
                    this.theController.selectMealPlan(mealPlan);
                    
                    while(Console.readBoolean("Edit another item? (y/n)")) {
                        final SelectWidget<MealPlanItem> itemSelector = new SelectWidget<>(mealPlan.itemList(), new MealPlanItemPrinter());
                        itemSelector.show();
                        MealPlanItem editItem = itemSelector.selectedElement();
                        
                        try {
                            final int quantity = Console.readInteger("New quantity:");
                            theController.changeQuantity(editItem, quantity);
                        }catch (IllegalArgumentException e) {
                            System.out.println("Invalid quantity!\n");
                        }
                    }
                    
                    this.theController.save();
                }
                
            }
            
        } catch (DataIntegrityViolationException e) {
            System.out.println("Error saving meal plan");
        }
        
        return false;
    }

    @Override
    public String headline() {
        return "Edit Meal Plan";
    }
    
}
