/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Bruno Stamm
 */
public class MealPlanItemPrinter implements Visitor<MealPlanItem>{

    @Override
    public void visit(MealPlanItem visitee) {
        System.out.println(visitee.toString());
    }

    @Override
    public void beforeVisiting(MealPlanItem visitee) {
    }

    @Override
    public void afterVisiting(MealPlanItem visitee) {
    }

   
    
}
