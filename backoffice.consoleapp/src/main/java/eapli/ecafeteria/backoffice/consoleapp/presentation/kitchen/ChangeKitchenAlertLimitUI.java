/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.ChangeKitchenAlertLimitController;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

public class ChangeKitchenAlertLimitUI extends AbstractUI {

    private final ChangeKitchenAlertLimitController theController = new ChangeKitchenAlertLimitController();

    protected ChangeKitchenAlertLimitController controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        System.out.println("Yellow Alert: "+this.controller().showCurrentKitchenAlertLimits().get(0)+"%\nRed Alert: "+this.controller().showCurrentKitchenAlertLimits().get(1)+"%");
        String answer = Console.readLine("\nDo you wish to change the limits? (yes or no)");
        if (answer.equals("yes")){
            int yellow = Console.readInteger("\nInsert the Yellow Alert percentual value:");
            int red = Console.readInteger("\nInsert the Red Alert percentual value:");
            this.controller().changeKitchenAlertLimits(yellow,red);
            
            System.out.println("\nKitchen Alert limits changed successfully");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Change Kitchen Alert Limit";
    }
}
