
package eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register;

import eapli.ecafeteria.application.MealDeliveryController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

/**
 * 
 *
 * @author Tiago Pereira 1140954
 */
public class MealDeliveryUI extends AbstractUI {
    private final MealDeliveryController theController = new MealDeliveryController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        
        String userId = Console.readLine("Username");
        
        return this.theController.deliverReservation(userId);
    }

    @Override
    public String headline() {
        return "Meal Delivery";
    }
}
