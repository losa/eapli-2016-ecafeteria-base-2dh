/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register;

import eapli.framework.actions.Action;

/**
 *
 * @author Tiago
 */
public class SeeAvailableMealsAction implements Action {
    
    @Override
    public boolean execute() {
        return new SeeAvailableMealsUI().show();
    }
    
}