package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.RegisterCookedMealsController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MealPrinter;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.util.Console;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Marcelo Oliveira
 */
public class RegisterCookedMealsUI extends AbstractUI {

    private final RegisterCookedMealsController theController = new RegisterCookedMealsController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        final Calendar day = this.theController.todayDate();
        final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        System.out.println("Today's Date: " + dateFormat.format(day.getTime()));
        System.out.println("\nMeal Types:");

        final Set<MealType> mealTypes = new HashSet<>();
        boolean show;
        do {
            show = showMeals(mealTypes);
        } while (!show);

        final List<Meal> listMeals = this.theController.listMealsFromDayAndType(day, mealTypes.iterator().next());
        if (listMeals == null || listMeals.isEmpty()) {
            System.out.println("\nThere are no meals for this day/meal type or meal plan is missing.");
        } else {
            showMeals(listMeals);
            this.theController.saveMealPlan();
            System.out.println("\nCooked quantities for the selected meals have been saved.");
        }

        return false;
    }

    private void showMeals(List<Meal> listMeals) {
        boolean exit = true;
        while (exit) {
            System.out.println("\n== Meals List =======================================================");
            System.out.printf("%-3s%-15s | %-30s | %-5s\n", "", "Date", "Dish - Type", "Meal");
            final SelectWidget<Meal> selector = new SelectWidget<>(listMeals, new MealPrinter());
            selector.show();
            final Meal theMeal = selector.selectedElement();
            if (theMeal != null) {
                try {
                    final int quantity = Console.readInteger("\nEnter the cooked quantity for this meal " + theMeal.description() + ": ");

                    if (this.theController.updateMealPlanItemCookedQuantity(theMeal, quantity)) {
                        System.out.println("\nCooked quantity registered for meal " + theMeal.description() + ".");
                    } else {
                        System.out.println("\nThere was a problem registering the cooked quantity for meal " + theMeal.description() + ".");
                    }
                    
                } catch (IllegalArgumentException e) {
                    System.out.println("Invalid quantity!\n");
                }
            } else {
                exit = false;
            }
        }
    }

    private boolean showMeals(final Set<MealType> mealTypes) {
        final Menu mealsMenu = buildMealsMenu(mealTypes);
        final MenuRenderer renderer = new VerticalMenuRenderer(mealsMenu);
        return renderer.show();
    }

    private Menu buildMealsMenu(final Set<MealType> mealTypes) {
        final Menu mealsMenu = new Menu();
        int counter = 1;
        for (final MealType mealType : this.theController.listMealType()) {
            mealsMenu.add(new MenuItem(counter++, mealType.name(), () -> mealTypes.add(mealType)));
        }
        return mealsMenu;
    }

    @Override
    public String headline() {
        return "Register Cooked Meals";
    }
}
