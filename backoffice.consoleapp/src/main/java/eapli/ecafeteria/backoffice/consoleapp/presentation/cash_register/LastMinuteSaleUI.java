/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register;

import eapli.ecafeteria.application.AddFundsController;
import eapli.ecafeteria.application.LastMinuteMealSaleController;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Tiago
 */
public class LastMinuteSaleUI extends AbstractUI {

    private final LastMinuteMealSaleController theController = new LastMinuteMealSaleController();
    
    CafeteriaUser cafUser;

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        int tentativas, i=0,j;
        String payment;
        Scanner read;
        List<Meal> meals =  theController.getAvailableMeals();
        for (Meal meal :meals) {
            System.out.println(i+". "+meal.description() +" price:"+meal.getPrice().getValue());
            i++;
        }
        if(meals.isEmpty()){
            System.out.println("There are no available meals!");
            return false;
        }
        for (j = 0; j < 3; j++) {
            System.out.println("Choose one of the available meals:");
            read = new Scanner(System.in);
            i= read.nextInt();
            if(i>0 && i< meals.size())
                break;
            System.out.println("Pick only from the available Meals u only have"+(3-j) +"tries");
        }
        if (j==3) {
            return false;
        }
        System.out.println("Are you a registered user?");
        System.out.println("1. Yes");
        System.out.println("2. No");
        int option = Console.readOption(1, 2,0);
        
        if (option==1) {
            System.out.println("Input your mecanographic number: ");
            read = new Scanner(System.in);
            payment=read.next();
            // permite 3 tentativas
            for (tentativas = 0; tentativas < 3; tentativas++) {
               
               if( theController.getUserById(payment))
                   break;

                System.out.println("Número mecanografico não existe! Insira novamente o seu número (tentativas -> "+(tentativas+1)+")");
                payment=read.next();
            }
            if(tentativas == 3){
                System.out.println("Atingido limite máximo de tentativas.");
                return false;
            }
        }
        else {
            System.out.println("Input your MB number: ");
            read = new Scanner(System.in);
            payment=read.next();
        }
        
        
        
               
        // FIX ME!! VERIFICAR SE A IMPLEMENTAÇÂO FUNCIONA DEVIDAMENTE E SE SERÀ NESSESSÀRIO APRESENTAR O MONTANTE ATUALIZADO DO USER.
        theController.lastMinuteSale(payment, i, option);
        System.out.println("Purchase made with sucess!");


        return true;
    }

    @Override
    public String headline() {
        return "Meal Delivery";
    }
}