package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.ShowDifferenceFromPredictionController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MealPlanItemDifferencePrinter;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ListWidget;
import eapli.util.Console;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Marcelo Oliveira
 */
public class ShowDifferenceFromPredictionUI extends AbstractUI {

    private final ShowDifferenceFromPredictionController theController = new ShowDifferenceFromPredictionController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        final Calendar day = Console.readCalendar("Date (dd-mm-yyyy): ");

        final List<MealPlanItem> listItems = this.theController.mealPlanItemsFromDay(day);

        if (listItems == null || listItems.isEmpty()) {
            System.out.println("\nThere are no meals for this day or meal plan is missing.");
            return false;
        }

        System.out.println("\n== Meals/Difference List =======================================================");
        System.out.printf("%-3s%-35s | %-10s | %-10s | %-5s\n", "", "Meal", "Cooked", "Consumed", "Difference");

        final ListWidget<MealPlanItem> lister = new ListWidget<>(listItems, new MealPlanItemDifferencePrinter());
        lister.show();

        System.out.println("");

        return false;
    }

    @Override
    public String headline() {
        return "Difference from prediction";
    }

}
