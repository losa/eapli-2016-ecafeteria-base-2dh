/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register;

import eapli.ecafeteria.application.MealDeliveryController;
import eapli.framework.actions.Action;
import eapli.framework.application.Controller;

/**
 *
 * @author Tiago
 */
public class AddFundsAction implements Action {
    
    @Override
    public boolean execute() {
        return new AddFundsUI().show();
    }
    
}