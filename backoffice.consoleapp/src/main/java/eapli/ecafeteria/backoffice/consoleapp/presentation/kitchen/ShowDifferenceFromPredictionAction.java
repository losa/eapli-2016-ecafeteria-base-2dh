package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.framework.actions.Action;

/**
 *
 * @author Marcelo Oliveira
 */
public class ShowDifferenceFromPredictionAction implements Action {

    @Override
    public boolean execute() {
         return new ShowDifferenceFromPredictionUI().show();
    }
    
}
