/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.PublishMenusController;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tiagogoncalves
 */
class PublishMenusUI extends AbstractUI{

    public PublishMenusUI() {
    }

    @Override
    protected boolean doShow() {
        PublishMenusController theController = new PublishMenusController();
        try{
            final Iterable<Menu> menus = theController.listMenus();
            
            final SelectWidget<Menu> selector = new SelectWidget<>(menus, new MenusPrinter());
            selector.show();
            final Menu theMenu = selector.selectedElement();
            if (theMenu != null) {
                
                if(theController.publishMenu(theMenu)){
                    System.out.println("Menu publicado");
                }else{
                    System.out.println("Menu não publicado");
                }
                
            }

            
        }   catch (DataIntegrityViolationException ex) {
            Logger.getLogger(PublishMenusUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public String headline() {
        return "Publish Menu";
    }
    
}
