/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.CreateMenusController;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author JoséDiogo
 */
class CreateMenusUI extends AbstractUI{
    
    private final CreateMenusController theController = new CreateMenusController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final Calendar begin = Console.readCalendar("Beginning of period(dd-mm-yyyy):");
        final Calendar end = Console.readCalendar("End of period(dd-mm-yyyy):");
        final String desc = Console.readLine("Description:");
        
        try {
            this.theController.createMenu(begin, end, desc);
        } catch (final DataIntegrityViolationException e) {
            System.out.println("Period invalid.");
        } catch (IllegalArgumentException ex){
            System.out.println("Failed to create menu.");
        }
        
        return false;
    }

    @Override
    public String headline() {
        return "Create Menus";
    }
    
}
