/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation;

import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.cafeteria.consoleapp.presentation.MyUserMenu;
import eapli.cafeteria.consoleapp.presentation.authz.LoginAction;
import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.application.ListOrganicUnitsController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.authz.AcceptRefuseSignupRequestAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.authz.AddUserUI;
import eapli.ecafeteria.backoffice.consoleapp.presentation.authz.DeactivateUserAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.authz.ListUsersAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria.AddOrganicUnitUI;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria.OrganicUnitPrinter;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register.AddFundsAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register.MealDeliveryAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register.OpenPosAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen.*;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.ActivateDeactivateDishTypeAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.ChangeDishTypeAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.CreateMealPlanAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.CreateMealsMenusAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.EditMealsMenusAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.ListDishTypeAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.RegisterDishAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.RegisterDishTypeAction;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.framework.actions.ReturnAction;
import eapli.framework.actions.ShowMessageAction;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.HorizontalMenuRenderer;
import eapli.framework.presentation.console.ListUI;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.ShowVerticalSubMenuAction;
import eapli.framework.presentation.console.SubMenu;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.framework.presentation.console.VerticalSeparator;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register.ClosePosAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register.LastMinuteSaleAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cash_register.SeeAvailableMealsAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.EditMealPlanAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.ListDishAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.PublishMealsMenusAction;
import java.util.Observable;
import java.util.Observer;

/**
 * TODO split this class in more specialized classes for each menu
 *
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI implements Observer {

    private static final int EXIT_OPTION = 0;

    // USERS
    private static final int ADD_USER_OPTION = 1;
    private static final int LIST_USERS_OPTION = 2;
    private static final int DEACTIVATE_USER_OPTION = 3;
    private static final int ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION = 4;

    // ORGANIC UNITS
    private static final int ADD_ORGANIC_UNIT_OPTION = 1;
    private static final int LIST_ORGANIC_UNIT_OPTION = 2;

    // SETTINGS
    private static final int SET_KITCHEN_ALERT_LIMIT_OPTION = 1;
    private static final int SET_USER_ALERT_LIMIT_OPTION = 2;

    // DISH TYPES
    private static final int DISH_TYPE_REGISTER_OPTION = 1;
    private static final int DISH_TYPE_LIST_OPTION = 2;
    private static final int DISH_TYPE_CHANGE_OPTION = 3;
    private static final int DISH_TYPE_ACTIVATE_DEACTIVATE_OPTION = 4;

    //DISH
    private static final int DISH_REGISTER_OPTION = 1;
    private static final int DISH_LIST_OPTION = 2;

    // KITCHEN MENU
    private static final int KITCHEN_CHECK_RESERVATIONS = 1;
    private static final int KITCHEN_REGISTER_COOKED_MEALS = 2;
    private static final int KITCHEN_REGISTER_UNSOLD_COOKED_MEALS = 3;
    private static final int KITCHEN_EXPORT_SERVED_MEALS_CSV = 4;
    private static final int KITCHEN_EXPORT_SERVED_MEALS_XML = 5;
    private static final int KITCHEN_DIFFERENCES_PREDICTION = 6;

    // MEALS MENU
    private static final int MEALS_CREATE_MENU = 1;
    private static final int MEALS_EDIT_MENU = 2;
    private static final int MEALS_PUBLISH_MENU = 3;

    // MEAL PLANS MENU
    private static final int MEAL_PLAN_CREATE_OPTION = 1;
    private static final int MEAL_PLAN_EDIT_OPTION = 2;
    private static final int MEAL_PLAN_GENERATE_OPTION = 3;

    //SALES MENU
    private static final int OPEN_POS_OPTION = 1;
    private static final int CLOSE_POS_OPTION = 2;
    private static final int MEAL_DELIVERY_OPTION = 3;
    private static final int LAST_MINUTE_MEAL_SALE_OPTION = 4;
    private static final int CHARGE_ACCOUNT_OPTION = 5;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int USERS_OPTION = 2;
    private static final int ORGANIC_UNITS_OPTION = 3;
    private static final int SETTINGS_OPTION = 4;
    private static final int DISH_TYPES_OPTION = 5;
    private static final int DISH_OPTION = 8;
    private static final int MEALS_OPTION = 7;
    private static final int CASHIER_OPTION = 2;
    private static final int MEAL_PLAN_OPTION = 9;

    // KITCHEN MAIN MENU
    private static final int KITCHEN_OPTION = 2;

    //Observable
    private Observer observer;

    @Override
    public boolean show() {
        if (AppSettings.instance().session() == null) {
            if (!new LoginAction().execute()) {
                return false;
            }
        }

        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer;
        if (AppSettings.instance().isMenuLayoutHorizontal()) {
            renderer = new HorizontalMenuRenderer(menu);
        } else {
            renderer = new VerticalMenuRenderer(menu);
        }
        return renderer.show();
    }

    @Override
    public String headline() {
        return "eCAFETERIA [@" + AppSettings.instance().session().authenticatedUser().id() + "]";
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        
        final Menu myUserMenu = new MyUserMenu();
        mainMenu.add(new SubMenu(MY_USER_OPTION, myUserMenu, new ShowVerticalSubMenuAction(myUserMenu)));

        if (!AppSettings.instance().isMenuLayoutHorizontal()) {
            mainMenu.add(VerticalSeparator.separator());
        }

        if (AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.Administer)) {
            final Menu usersMenu = buildUsersMenu();
            mainMenu.add(new SubMenu(USERS_OPTION, usersMenu, new ShowVerticalSubMenuAction(usersMenu)));
            final Menu organicUnitsMenu = buildOrganicUnitsMenu();
            mainMenu.add(new SubMenu(ORGANIC_UNITS_OPTION, organicUnitsMenu,
                    new ShowVerticalSubMenuAction(organicUnitsMenu)));
            final Menu settingsMenu = buildAdminSettingsMenu();
            mainMenu.add(new SubMenu(SETTINGS_OPTION, settingsMenu, new ShowVerticalSubMenuAction(settingsMenu)));
        } else if (AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.ManageKitchen)) {
            final Menu kitchenMenu = buildKitchenMenu();
            mainMenu.add(new SubMenu(KITCHEN_OPTION, kitchenMenu, new ShowVerticalSubMenuAction(kitchenMenu)));
        } else if (AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.ManageMenus)) {
            final Menu myDishTypeMenu = buildDishTypeMenu();
            mainMenu.add(new SubMenu(DISH_TYPES_OPTION, myDishTypeMenu, new ShowVerticalSubMenuAction(myDishTypeMenu)));
            final Menu myMealsMenu = buildMealsMenu();
            mainMenu.add(new SubMenu(MEALS_OPTION, myMealsMenu, new ShowVerticalSubMenuAction(myMealsMenu)));
            final Menu myDishMenu = buildDishMenu();
            mainMenu.add(new SubMenu(DISH_OPTION, myDishMenu, new ShowVerticalSubMenuAction(myDishMenu)));
            final Menu myMealPlanMenu = buildMealPlanMenu();
            mainMenu.add(new SubMenu(MEAL_PLAN_OPTION, myMealPlanMenu, new ShowVerticalSubMenuAction(myMealPlanMenu)));
        } else if (AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.Sale)) {
            final Menu myCashierMenu = buildCashierMenu();
            mainMenu.add(new SubMenu(CASHIER_OPTION, myCashierMenu, new ShowVerticalSubMenuAction(myCashierMenu)));
        }

        if (!AppSettings.instance().isMenuLayoutHorizontal()) {
            mainMenu.add(VerticalSeparator.separator());
        }

        mainMenu.add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));

        return mainMenu;
    }

    private Menu buildAdminSettingsMenu() {
        final Menu menu = new Menu("Settings >");

        menu.add(new MenuItem(SET_KITCHEN_ALERT_LIMIT_OPTION, "Set kitchen alert limit",
                new ChangeKitchenAlertLimitAction()));
        menu.add(new MenuItem(SET_USER_ALERT_LIMIT_OPTION, "Set users' alert limit",
                new ShowMessageAction("Not implemented yet")));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildOrganicUnitsMenu() {
        final Menu menu = new Menu("Organic units >");

        menu.add(new MenuItem(ADD_ORGANIC_UNIT_OPTION, "Add Organic Unit", () -> {
            return new AddOrganicUnitUI().show();
        }));
        menu.add(new MenuItem(LIST_ORGANIC_UNIT_OPTION, "List Organic Unit", () -> {
            // example of using the generic list ui from the framework
            new ListUI<OrganicUnit>(new ListOrganicUnitsController().listOrganicUnits(), new OrganicUnitPrinter(),
                    "Organic Unit").show();
            return false;
        }));
        // TODO add other options for Organic Unit management
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildUsersMenu() {
        final Menu menu = new Menu("Users >");

        menu.add(new MenuItem(ADD_USER_OPTION, "Add User", () -> {
            return new AddUserUI().show();
        }));
        menu.add(new MenuItem(LIST_USERS_OPTION, "List all Users", new ListUsersAction()));
        menu.add(new MenuItem(DEACTIVATE_USER_OPTION, "Deactivate User", new DeactivateUserAction()));
        menu.add(new MenuItem(ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION, "Accept/Refuse Signup Request",
                new AcceptRefuseSignupRequestAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildDishTypeMenu() {
        final Menu menu = new Menu("Dish Type >");

        menu.add(new MenuItem(DISH_TYPE_REGISTER_OPTION, "Register new Dish Type", new RegisterDishTypeAction()));
        menu.add(new MenuItem(DISH_TYPE_LIST_OPTION, "List all Dish Type", new ListDishTypeAction()));
        menu.add(new MenuItem(DISH_TYPE_CHANGE_OPTION, "Change Dish Type description", new ChangeDishTypeAction()));
        menu.add(new MenuItem(DISH_TYPE_ACTIVATE_DEACTIVATE_OPTION, "Activate/Deactivate Dish Type",
                new ActivateDeactivateDishTypeAction()));

        return menu;
    }

    private Menu buildDishMenu() {
        final Menu menu = new Menu("Dish Menu >");

        menu.add(new MenuItem(DISH_REGISTER_OPTION, "Register new Dish", new RegisterDishAction()));
        menu.add(new MenuItem(DISH_LIST_OPTION, "List all Dish", new ListDishAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildKitchenMenu() {

        final Menu menu = new Menu("Kitchen Menu >");
        menu.add(new MenuItem(KITCHEN_CHECK_RESERVATIONS, "Check reservations", new ShowReservationAction()));
        menu.add(new MenuItem(KITCHEN_REGISTER_COOKED_MEALS, "Register cooked meals", new RegisterCookedMealsAction()));
        menu.add(new MenuItem(KITCHEN_REGISTER_UNSOLD_COOKED_MEALS, "Register unsold cooked meals", new RegisterUnsoldCookedMealsAction()));

        menu.add(new MenuItem(KITCHEN_EXPORT_SERVED_MEALS_CSV, "Export served Meals (CSV)", new ExportServedMealsToCSVAction()));
        menu.add(new MenuItem(KITCHEN_EXPORT_SERVED_MEALS_XML, "Export served Meals (XML)", new ExportServedMealsToXMLAction()));
        menu.add(new MenuItem(KITCHEN_DIFFERENCES_PREDICTION, "Show differences compared to predictions ", new ShowDifferenceFromPredictionAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        
        return menu;
    }

    private Menu buildMealsMenu() {

        final Menu menu = new Menu("Meals Menu >");
        menu.add(new MenuItem(MEALS_CREATE_MENU, "Create menu", new CreateMealsMenusAction()));
        menu.add(new MenuItem(MEALS_EDIT_MENU, "Edit menu", new EditMealsMenusAction()));
        menu.add(new MenuItem(MEALS_PUBLISH_MENU, "Publish menu", new PublishMealsMenusAction()));

        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

private Menu buildCashierMenu() {
        final Menu menu = new Menu("Cashier Menu >");
        if (AppSettings.instance().session().getPOS() == null) {
            menu.add(new MenuItem(OPEN_POS_OPTION, "Open POS", new OpenPosAction()));
        } else {
            menu.add(new MenuItem(CLOSE_POS_OPTION, "Close POS", new ClosePosAction()));
            menu.add(new MenuItem(MEAL_DELIVERY_OPTION, "Deliver meal", new MealDeliveryAction()));
            menu.add(new MenuItem(LAST_MINUTE_MEAL_SALE_OPTION, "Last Minute Meal Sale",new LastMinuteSaleAction()));
            menu.add(new MenuItem(CHARGE_ACCOUNT_OPTION, "Charge Account ", new AddFundsAction()));
            //Prints the available meals
            new SeeAvailableMealsAction().execute();

        }
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildMealPlanMenu() {
        final Menu menu = new Menu("Meal Plan Menu >");

        menu.add(new MenuItem(MEAL_PLAN_CREATE_OPTION, "Create meal plan", new CreateMealPlanAction()));
        menu.add(new MenuItem(MEAL_PLAN_EDIT_OPTION, "Edit meal plan", new EditMealPlanAction()));
//        menu.add(new MenuItem(MEAL_PLAN_GENERATE_OPTION, "Generate meal plan", new GenerateMealPlanAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("\nAlert " + arg);
    }

}
