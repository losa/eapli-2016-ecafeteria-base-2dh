/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterDishController;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Price;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;

/**
 *
 * @author i131463
 */
public class RegisterDishUI extends AbstractUI {
    private final RegisterDishController theController = new RegisterDishController();

    protected Controller controller() {
        return this.theController;
    }
    
    @Override
    protected boolean doShow() {
        final String name = Console.readLine("Dish Name:");
        
        final Iterable<DishType> allDishTypes = theController.listDishTypes();
        
        DishType dishType = null;
                
        if (!allDishTypes.iterator().hasNext()) {
            System.out.println("There is no registered Dish Type");
        } else {
            System.out.println("");
            System.out.println("Dish Type:");
            final SelectWidget<DishType> selector = new SelectWidget<>(allDishTypes, new DishTypePrinter());
            selector.show();
            dishType = selector.selectedElement();
        }
        
        final long kcal = (long)Console.readDouble("Dish calories(kcal):");
        final int fatPer = Console.readInteger("Dish fat percentage:");
        final Price price = new Price(Console.readDouble("Dish price:"));

        try {
            this.theController.registerDish(name, dishType, kcal, fatPer, price);
            System.out.println("Success");
        } catch (final DataIntegrityViolationException e) {
            System.out.println("That acronym is already in use.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Register Dish";
    }
    
    
}